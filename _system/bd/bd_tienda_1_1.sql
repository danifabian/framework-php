/*USE prod_skate_shop;*/
DROP TABLE if exists lineas_fac;
DROP TABLE if exists tablas_completas;
DROP TABLE if exists noticias;
DROP TABLE if exists facturas;
DROP TABLE if exists telefonos;
DROP TABLE if exists usuarios;
DROP TABLE if exists productos;
DROP TABLE if exists clientes;
DROP TABLE if exists pueblos;
DROP TABLE if exists categorias;
DROP TABLE if exists provincias;
DROP TABLE if exists marcas;

CREATE TABLE marcas (
  id_marca INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(45) NULL,
  img_url VARCHAR(255) NULL,
  PRIMARY KEY(id_marca)
);

CREATE TABLE provincias (
  id_provincia INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(45) NULL,
  PRIMARY KEY(id_provincia)
);

CREATE TABLE categorias (
  id_categoria INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(45) NULL,
  PRIMARY KEY(id_categoria)
);

CREATE TABLE pueblos (
  id_pueblo INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_provincia INTEGER UNSIGNED NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  cod_postal INTEGER(5) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY(id_pueblo),
  FOREIGN KEY(id_provincia)
    REFERENCES provincias(id_provincia)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE clientes (
  id_cliente INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_pueblo INTEGER UNSIGNED NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  apellidos VARCHAR(45) NULL,
  direccion VARCHAR(45) NULL,
  fecha_nacimiento DATE NULL,
  fecha_registro DATE NULL,
  email VARCHAR(100) NOT NULL,
  PRIMARY KEY(id_cliente),
  FOREIGN KEY(id_pueblo)
    REFERENCES pueblos(id_pueblo)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE productos (
  id_producto INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_marca INTEGER UNSIGNED NOT NULL,
  id_categoria INTEGER UNSIGNED NOT NULL,
  fecha_ingreso DATETIME NOT NULL,
  descripcion VARCHAR(2048) NOT NULL,
  stock INTEGER UNSIGNED NOT NULL,
  precio FLOAT NOT NULL,
  modelo VARCHAR(45) NOT NULL,
  dimensiones VARCHAR(45) NULL,
  peso INTEGER UNSIGNED NULL,
  concavo VARCHAR(45) NULL,
  color VARCHAR(45) NOT NULL,
  img_url VARCHAR(255) NULL,
  destacado INTEGER UNSIGNED NOT NULL,
  tipo ENUM('tabla', 'ejes', 'ruedas','cojinetes','camisetas','pantalones','sudaderas','lija','goma','tornillo','herramienta','cordones','proteccion') NULL,
  PRIMARY KEY(id_producto, id_marca, id_categoria),
  FOREIGN KEY(id_categoria)
    REFERENCES categorias(id_categoria)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
  FOREIGN KEY(id_marca)
    REFERENCES marcas(id_marca)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE usuarios (
  username VARCHAR(45) NOT NULL,
  id_cliente INTEGER UNSIGNED NOT NULL,
  pass VARCHAR(45) NOT NULL,
  role enum('admin', 'gestor', 'usuario') NOT NULL,
  PRIMARY KEY(username),
  FOREIGN KEY(id_cliente)
    REFERENCES clientes(id_cliente)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE telefonos (
  id_telefono INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_cliente INTEGER UNSIGNED NOT NULL,
  numero INTEGER UNSIGNED NULL,
  PRIMARY KEY(id_telefono),
  FOREIGN KEY(id_cliente)
    REFERENCES clientes(id_cliente)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE facturas (
  id_factura INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_cliente INTEGER UNSIGNED NOT NULL,
  codigo_pedido VARCHAR(15) NOT NULL,
  fecha DATETIME NULL,
  dto FLOAT NULL,
  PRIMARY KEY(id_factura),
  FOREIGN KEY(id_cliente)
    REFERENCES clientes(id_cliente)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE noticias (
  id_noticia INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  titulo VARCHAR(200) NULL,
  cuerpo VARCHAR(255) NULL,
  fecha_registro DATETIME NULL,
  fecha_modificacion DATETIME NULL,
  img_url VARCHAR(255) NULL,
  PRIMARY KEY(id_noticia),
  FOREIGN KEY(username)
    REFERENCES usuarios(username)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

CREATE TABLE tablas_completas (
  id_tabla INTEGER UNSIGNED NOT NULL,
  id_items_tabla INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(id_tabla, id_items_tabla),
  FOREIGN KEY(id_tabla)
    REFERENCES productos(id_producto)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(id_items_tabla)
    REFERENCES productos(id_producto)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE lineas_fac (
  id_linea INTEGER UNSIGNED NOT NULL,
  id_factura INTEGER UNSIGNED NOT NULL,
  id_producto INTEGER UNSIGNED NOT NULL,
  cant INTEGER UNSIGNED NULL,
  dto FLOAT NULL,
  precio FLOAT NULL,
  PRIMARY KEY(id_linea, id_factura),
  FOREIGN KEY(id_producto)
    REFERENCES productos(id_producto)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(id_factura)
    REFERENCES facturas(id_factura)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
);

ALTER TABLE lineas_fac ENGINE=InnoDB;
ALTER TABLE tablas_completas ENGINE=InnoDB;
ALTER TABLE noticias ENGINE=InnoDB;
ALTER TABLE facturas ENGINE=InnoDB;
ALTER TABLE telefonos ENGINE=InnoDB;
ALTER TABLE usuarios ENGINE=InnoDB;
ALTER TABLE productos ENGINE=InnoDB;
ALTER TABLE clientes ENGINE=InnoDB;
ALTER TABLE pueblos ENGINE=InnoDB;
ALTER TABLE categorias ENGINE=InnoDB;
ALTER TABLE provincias ENGINE=InnoDB;
ALTER TABLE marcas ENGINE=InnoDB;

/* Inserts */
INSERT INTO marcas (nombre, img_url) VALUES("Alien Workshop",'public/upload/marcas/alien.png'); /*  1  */
INSERT INTO marcas (nombre, img_url) VALUES("Circa",'public/upload/marcas/circa.jpg');/*  2  */
INSERT INTO marcas (nombre, img_url) VALUES("DC Shoes",'public/upload/marcas/dc_shoes.png');/*  3  */
INSERT INTO marcas (nombre, img_url) VALUES("Chocolate",'public/upload/marcas/chocolate.png');/*  4 */
INSERT INTO marcas (nombre, img_url) VALUES("Jart",'public/upload/marcas/jart.png');/*  5  */
INSERT INTO marcas (nombre, img_url) VALUES("Santa Cruz",'public/upload/marcas/santa_cruz.png');/*  6  */
INSERT INTO marcas (nombre, img_url) VALUES("Blind",'public/upload/marcas/blind.png');/*  7  */
INSERT INTO marcas (nombre, img_url) VALUES("Toy Machine",'public/upload/marcas/toy-machine.png');/*  8  */
INSERT INTO marcas (nombre, img_url) VALUES("Nike",'public/upload/marcas/nike.png');/*  9  */
INSERT INTO marcas (nombre, img_url) VALUES("Volcom",'public/upload/marcas/volcom.png');/*  10  */
INSERT INTO marcas (nombre, img_url) VALUES ("Hawk",'public/upload/marcas/hawk.gif');/*  11  */
INSERT INTO marcas (nombre, img_url) VALUES ("Venture",'public/upload/marcas/venture.png');/*  12  */
INSERT INTO marcas (nombre, img_url) VALUES ("Globe",'public/upload/marcas/globe.png');/*  13  */
INSERT INTO marcas (nombre, img_url) VALUES ("Girl",'public/upload/marcas/girl.png');/*  14  */
INSERT INTO marcas (nombre, img_url) VALUES ("Indenpendent",'public/upload/marcas/independent.png');/*  15  */
INSERT INTO marcas (nombre, img_url) VALUES ("Bones",'public/upload/marcas/bones.png');/*  16  */
INSERT INTO marcas (nombre, img_url) VALUES ("Bear",'public/upload/marcas/bear.png');/*  17  */
INSERT INTO marcas (nombre, img_url) VALUES ("KINGPIN",'public/upload/marcas/KINGPIN.png');/*  18  */
INSERT INTO marcas (nombre, img_url) VALUES ("MASTERDIS",'public/upload/marcas/masterdis.jpg');/*  19  */
INSERT INTO marcas (nombre, img_url) VALUES ("PIG",'public/upload/marcas/pig.png');/*  20  */


INSERT INTO categorias (nombre)	VALUES("Skateboard");
INSERT INTO categorias (nombre)	VALUES("Longboard");
INSERT INTO categorias (nombre)	VALUES("Accesorio");
INSERT INTO categorias (nombre)	VALUES("Hombre");
INSERT INTO categorias (nombre)	VALUES("Mujer");

/* productos */
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (1,1, '2015-11-11:16:30:00', 'BAKER skateboards, marca americana fundada en el año 2000 por el mismo Andrew Reynolds.', 15, 67.00, 'Baker', '8.1" x 32.125"', null, 'Medium concave', 'Negro', 'public/upload/productos/skates/tabla/alien_baker.png', 0, 'tabla');/* Skateboard - Tabla - Alien workshop - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (14,1, '2015-11-11:16:35:00', 'Sponsor de Marcelino Castro, José Noro, Manuel Álvarez y Javier Suárez. Así que queremos dejaros algunas tablas de la nueva colección. Girl Skateboards.', 5, 60.99, 'Mullen 1', '7.875" x 32.125"', null, 'Concavo medio', 'Multicolor', 'public/upload/productos/skates/tabla/girl_imagine.png', 1, 'tabla');/* Skateboard - Tabla - Globe - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (12,1, '2015-11-10:16:40:00', 'EJES VENTURE HAPPY TO SERVE YOU LOW 5.25', 8, 52.00, 'Gate gigante', '5.25', 366, null, 'Azul', 'public/upload/productos/skates/ejes/eje_venture.jpg', 0, 'ejes');/* Skateboard - Ejes - venture - no destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (15,1, '2015-10-11:16:30:00', 'Ejes Independent 149 OGBG Black Chrome standar stage 11(Pareja)', 26, 70, 'BLACK CHROME STANDAR STAGE ', '8.0" - 8.5"', 340, null, 'Negro', 'public/upload/productos/skates/ejes/eje_independent.png', 1, 'ejes');/* Skateboard - Ejes - Venture - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (4,1, '2015-10-08:18:54:00', 'CHOCOLATE 52mm Sumi Chunk', 7, 42.99, 'Sumi Chunk', '52mm', 125, null, 'Multicolor', 'public/upload/productos/skates/ruedas/rueda_chocolate.jpg', 0, 'ruedas');/* Skateboard - Ruedas - no destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (14,1, '2015-10-25:11:25:00', '54mm 101A Glitch Mod', 2, 42.95, 'Glitch Mode', '54mm', 125, null, 'Rosa', 'public/upload/productos/skates/ruedas/rueda_girl.jpg', 1, 'ruedas');/* Skateboard - ruedas - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (16,3, '2015-11-10:15:30:00', 'cojinetes de alta calidad, inclusive BONES Speed Cream,8 anillos de velocidad, inclusive 4 casquillo espaciador', 5, 84.95, 'Swiss 6 Balls', null, null, null, 'Blanco', 'public/upload/productos/skates/cojinetes/bones_cojinetes_super_swiss.jpg', 0, 'cojinetes');/* Accesorios - cojinetes - no destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (15,3, '2015-11-09:17:30:00', 'RODAMIENTOS INDEPENDENT BEARINGS ABEC 7', 0, 23.00, 'Abec 7', null, null, null, 'Blanco', 'public/upload/productos/skates/cojinetes/independent_abec7.jpg', 1, 'cojinetes');/* Accesorios - cojinetes - no destacado*/

 /* long*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (13,2, '2015-11-04:18:30:00', 'The Kaguya is a bamboo top, 8-ply maple longboard, perfect for cruising around town. 43" long with a kick tail, concave and 50 grit clear grip that shows off the beauty of a top graphic.', 25, 70.95, 'KAGUYA', '43"', null, 'Concavo mediano', 'Multicolor', 'public/upload/productos/skates/tabla/globe-kaguya.jpg', 1, 'tabla');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (13,2, '2015-11-11:16:35:00', 'The Spearpoint Bamboo 40" longboard deck from Globe is an evolution of Globe popular Geminon Series, this deck is great for casual or advanced riders, the Spearpoint features a bamboo top and bottom and the drop through construction gives you the lowest possible center of gravity and in turn the most stability.', 5, 73.99, 'SPEARPOINT ', '40"', null, 'Concavo medio', 'Multicolo', 'public/upload/productos/skates/tabla/globe-spearpoint.jpg', 1, 'tabla');/* Skateboard - Tabla - Globe - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (17,2, '2015-11-10:16:40:00', 'The Kodiak longboard trucks feature forged hangers and baseplates that represent Bears lightest, strongest construction to date. Machined steel axles ensure precise alignment to prevent unwanted wheel play, while an entirely redesigned bushing seat improves stability and response. Experience Bear latest performance evolution with the Kodiak.', 8, 158.00, 'Kodiak', '9.75"', null, null, 'Rojo', 'public/upload/productos/skates/ejes/bear-kodiak.jpg', 0, 'ejes');/* Skateboard - Ejes - venture - no destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (17,2, '2015-11-10:16:40:00','The Grizzly 852 Gen 5 longboard trucks from Bear have been revamped and redesigned to focus on strength, versatility and weight reduction. Improvements include a stronger heat treated axle with a revised and less restrictive bushing seat. The baseplate now features an 8-hole mounting system to accommodate both old and new-school drill patterns.', 26, 158.99, 'Grizzly', '9.75', null, null, 'Rojo', 'public/upload/productos/skates/ejes/bear-grizzly.jpg', 1, 'ejes');/* Skateboard - Ejes - Venture - destacado*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (4,2, '2015-10-08:18:54:00', 'CHOCOLATE 52mm Sumi Chunk', 17, 42.99, 'Sumi Chunk', '52mm', 125, null, 'Multicolor', 'public/upload/productos/skates/ruedas/chocolate-51mm.jpg', 0, 'ruedas');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (14,2, '2015-10-25:11:25:00', 'GIRL 58mm 80A Big Fish', 12, 37.95, 'Big Fish', '58mm', 125, null, 'Rojo', 'public/upload/productos/skates/ruedas/girl_big_fish.jpg', 1, 'ruedas');

        /* moda */
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (9,4, '2015-12-09:17:30:00', 'Cuello redondo - Algodón orgánico', 15, 29.99, 'Dri-Fit Geo Dye Icon', "M", null, null, 'Negro', 'public/upload/productos/ropa/camisetas/camiseta_nike_negro.jpg', 0, 'camisetas');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (9,4, '2015-12-09:17:30:00', 'Pantalón chino - Pantalones con cierre de cremallera', 25, 69.99, 'FTM 5 Pocket', "M", null, null, 'Negro', 'public/upload/productos/ropa/pantalones/pantalones_nike_negro_1.jpg', 1, 'pantalones');

INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (9,5, '2015-12-09:17:30:00', 'Material transpirable, Material elástico - Escote en V', 5, 24.99, 'Pro V-Neck', "M", null, null, 'Negro', 'public/upload/productos/ropa/camisetas/camisetas_nike_negro.jpg', 0, 'camisetas');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (9,5, '2015-12-09:17:30:00', 'Pantalón de tela - Cintura elástica', 5, 34.99, 'Helsinki Winter', "M", null, null, 'Negro', 'public/upload/productos/ropa/pantalones/pantalones_nike_negro.jpg', 1, 'pantalones');

        /*accesorios*/
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (18,3, '2015-12-09:17:30:00', 'KINGPIN Coarse 11" x 43"', 25, 7.99, 'Coarse', null, null, null, 'Negro', 'public/upload/productos/accesorios/lija/lija_negra.jpg', 0, 'lija');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (16,3, '2015-12-09:17:30:00', 'BONES Bushings Set Hard - Accesorio para skateboard', 15, 11.99, 'Bushings Set Hard', null, null, null, 'Negro', 'public/upload/productos/accesorios/gomas/goma_bones_hard.jpg', 0, 'goma');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (16,3, '2015-12-09:17:30:00', 'BONES Spacer - Tornillos', 55, 0.99, 'Spacer', null, null, null, 'Gris', 'public/upload/productos/accesorios/tornillos/tornillo_bones_spacer.jpg', 0, 'tornillo');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (20,3, '2015-12-09:17:30:00', 'Herramienata de la marca PIG', 5, 21.90, 'Tool White', null, null, null, 'Blanco', 'public/upload/productos/accesorios/tools/herramienta_pig.jpg', 0, 'herramienta');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (11,3, '2015-12-09:17:30:00', 'certificado EN 1078 clase B, cierre de click, mate, sistema de ventilación, certificado ASTM F 2040 standard', 5, 30.99, 'Free Ride', null, null, null, 'Negro', 'public/upload/productos/accesorios/protecciones/proteccion_casco_ride.jpg', 0, 'proteccion');
INSERT INTO productos (id_marca, id_categoria, fecha_ingreso, descripcion, stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)
	VALUES (19,3, '2015-12-09:17:30:00', 'MASTERDIS Tube Laces 140cm', 5, 2.99, 'Tube Laces', null, null, null, 'Blanco', 'public/upload/productos/accesorios/cordones/cordones_masterdis_blanco.jpg', 0, 'cordones');


/* tablas completas */
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (1,1);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (1,3);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (1,5);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (1,7);

INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (2,2);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (2,4);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (2,6);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (2,8);

/* long */
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (9,9);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (9,11);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (9,13);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (9,15);

INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (10,10);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (10,12);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (10,14);
INSERT INTO tablas_completas (id_tabla, id_items_tabla) VALUES (10,16);
/* provincias */
INSERT INTO provincias (nombre) VALUES ("BARCELONA");  
INSERT INTO provincias (nombre) VALUES ("GIRONA");  
INSERT INTO provincias (nombre) VALUES ("TARRAGONA");  
INSERT INTO provincias (nombre) VALUES ("LLEIDA");  
/* pùeblos */
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (1,"Barcelona", 08028);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (1,"Barbera del Valles", 08210);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (1,"Sabadell", 08201);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (2, "Olot", 08203);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (2, "Gerona", 17005);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (2, "Palamós", 17007);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (3, "Amposta", 43870);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (3, "Tarragona", 43007);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (3, "Torredembarra", 43830);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (4, "Agramunt", 25310);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (4, "Lérida", 25003);
INSERT INTO pueblos (id_provincia,nombre,cod_postal) VALUES (4, "Seo de Urgel", 25700);
/* cliente */
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES (1, "Dani", "Fabian", "C/ Falsa 123", '1990-07-02', '2015-11-12', 'dafaos@gmail.com');
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES (1, "Gestor", "Gestor", "C/ Melainveto 35", '1992-04-12', '2015-11-12', 'gestor@example.com');
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES (1, "User", "User", "C/ user 45", '1995-06-23', '2015-11-12', 'user@example.com');
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES (1, 'Sergio', 'Gallo', 'C/ Piruleta 123', '1990-07-02', '2015-11-12', 'sergioux@gmail.com');
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES(1, 'Sergio', 'Gallo', 'C/ Piruleta 123', '1990-07-02', '2015-11-12', 'sergioux@gmail.com');
INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)
    VALUES(1, 'Arturo', 'Cardenas', 'C/ Gominola 123', '1990-07-02', '2015-11-12', 'arturoux@gmail.com');
/* usuarios */
/* El password se a hasheado con clave fija. Contenido de cifrado: password */
INSERT INTO usuarios (username, id_cliente, pass, role)
    VALUE ("Aspir-ina", 1, '86c356c9517738bd588f30a77e0ed0b6f3007662', 'admin');
INSERT INTO usuarios (username, id_cliente, pass, role)
    VALUE ("Gestor", 2, '86c356c9517738bd588f30a77e0ed0b6f3007662', 'gestor');
INSERT INTO usuarios (username, id_cliente, pass, role)
    VALUE ("Usuario", 3, '86c356c9517738bd588f30a77e0ed0b6f3007662', 'usuario');
    
/* noticias probar paginacion */
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 1', 'Este es el cuerpo de la noticia 1', '2015-11-12', '2015-11-12', 'public/upload/noticias/default/2.gif');
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 2', 'Este es el cuerpo de la noticia 2', '2015-11-12', '2015-11-12', 'public/upload/noticias/default/2.gif');    
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 3', 'Este es el cuerpo de la noticia 3', '2015-11-12:13:22:00', '2015-11-12:13:22:00', 'public/upload/noticias/default/2.gif');
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 4', 'Este es el cuerpo de la noticia 4', '2015-11-12:13:30:00', '2015-11-12:13:30:00', 'public/upload/noticias/default/2.gif');    
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 5', 'Este es el cuerpo de la noticia 4', '2015-11-13', '2015-11-13:15:30:00', 'public/upload/noticias/default/2.gif');    
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 6', 'Este es el cuerpo de la noticia 4', '2015-11-13:15:40:00', '2015-11-13:15:40:00', 'public/upload/noticias/default/2.gif');    
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 7', 'Este es el cuerpo de la noticia 4', '2015-11-13:15:45:00', '2015-11-13:15:45:00', 'public/upload/noticias/default/2.gif');    
INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) 
    VALUES ('Aspir-ina', 'Titulo 8', 'Este es el cuerpo de la noticia 4', '2015-11-13:15:43:00', '2015-11-13:15:43:00', 'public/upload/noticias/default/2.gif');    


INSERT INTO facturas (id_factura, id_cliente,codigo_pedido, fecha, dto) 
    VALUES (1, 4, '11111', '2015-11-12 18:35:00', 5);
INSERT INTO facturas (id_factura, id_cliente,codigo_pedido, fecha, dto) 
    VALUES(2, 5, '22222', '2015-12-12 14:23:00', 10);
INSERT INTO facturas (id_factura, id_cliente,codigo_pedido, fecha, dto) 
    VALUES(3, 6, '33333', '2015-12-10 09:15:00', 5);

INSERT INTO lineas_fac (id_linea, id_factura, id_producto, cant, dto, precio)
    VALUES(1, 1, 1, 2, 5, 60.99);
INSERT INTO lineas_fac (id_linea, id_factura, id_producto, cant, dto, precio)
    VALUES(1, 3, 2, 1, 5, 60.99);
INSERT INTO lineas_fac (id_linea, id_factura, id_producto, cant, dto, precio)
    VALUES(2, 1, 2, 1, 5, 60.99);

INSERT INTO telefonos(id_cliente,numero)
    VALUES(1,999999999);
INSERT INTO telefonos(id_cliente,numero)
    VALUES(2,999999998);
INSERT INTO telefonos(id_cliente,numero)
    VALUES(3,999999997);
INSERT INTO telefonos(id_cliente,numero)
    VALUES(4,999999995);
INSERT INTO telefonos(id_cliente,numero)
    VALUES(5,999999996);
INSERT INTO telefonos(id_cliente,numero)
    VALUES(6,999999994);