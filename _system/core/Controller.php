<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
* Controlador principal.
 * Tiene metodos los cuales facilitan bastante la programación
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
abstract class Controller {
    /**
     *
     * @var type $_view Heredar de view para poder renderizar las vistas
     */
    protected $_view;
    /**
     *
     * @var type $_registry 
     */
    private $_registry;
        
    //Obligar a todas las clases que hereden de esta a tener un método index()
    abstract public function index();
    
    public function __construct() {
        $this->_registry = Registry::getInstancia();
        
        $this->_view = new View($this->_registry->_request);
    }
    
    /**
     * 
     * @param type $modelo
     * @return \modelo
     * @throws Exception
     */
    protected function loadModel($modelo){
        $modelo = $modelo . 'Model';
        $rutaModelo = APPPATH . 'models' . DS . $modelo . '.php';
        
        if(is_readable($rutaModelo)){
            require_once $rutaModelo;
            $modelo = new $modelo;
            return $modelo;
        }
        else{
            throw new Exception('ERROR: Modelo no encontrado');
        }
    }
    
    /**
     * Buscar y cargar una libreria
     * @param type $libreria
     */
    protected function getLibrary($libreria){
        $rutaLibreria = LIBS_PATH . $libreria . '.php';
        
        if(is_readable($rutaLibreria)){
            require_once $rutaLibreria;
        } 
        else{
            throw new Exception("ERROR: Libreria no existe o error de nombre");
        }
    }
    
    protected function getPostParam($clave){
        if(isset($_POST[$clave])){
            return $_POST[$clave];
        }
    }
    
    /**
     * Filtra parametros enviados por post
     * @param type $key
     * @return string
     */
    protected function getPostText($key){
        if(isset($_POST[$key]) && !empty($_POST[$key])){
            $_POST[$key] = htmlspecialchars($_POST[$key], ENT_QUOTES);
            return $_POST[$key];
        }
        return '';
    }
    
    /**
     * Filtrar datos enteros recibidos por post
     * @param type $key
     * @return string
     */
    protected function getPostInt($key){
        if(isset($_POST[$key]) && !empty($_POST[$key])){
            $_POST[$key] = filter_input(INPUT_POST, $key, FILTER_VALIDATE_INT);
            return $_POST[$key];
        }
        return '';
    }
    
    /* Filtrar enteros */
    protected function filtrarInt($value){
        $val = (int) $value;
        if(is_int($val)){
            return $val;
        }
        else{
            return 0;
        }
    }
    
    protected function getAlphaNum($clave){
        if(isset($_POST[$clave]) && !empty($_POST[$clave])){
            $_POST[$clave] = (string) preg_replace('/[^A-Z0-9_]/i', '', $_POST[$clave]);
            return trim($_POST[$clave]);
        }
    }
    
    /**
     * Filtrado de elementos para evitar ataques sql
     * @param type $clave
     * @return type
     */
    /*protected function getSql($clave){
        if(isset($_POST[$clave]) && !empty($_POST[$clave])){
            $_POST[$clave] = strip_tags($_POST[$clave]);
            
            if(!get_magic_quotes_gpc()){
                //$_POST[$clave] = mysql_real_escape_string($_POST[$clave]);
                
                $_POST[$clave] = mysqli_real_escape_string($_POST[$clave], '');
                
            }
            return trim($_POST[$clave]);
        }
    }*/
    
    /**
     * Filtrado de elementos para evistar ataques sql
     * reemplaza la funcion mysql_escape_string (Actualmente eliminada) 
     * y mysql_real_escape_string(Tambien deprecated)
     * @param type $clave
     * @return type
     */
    function mysql_escape_mimic($clave) { 
        if(isset($_POST[$clave]) && !empty($_POST[$clave])){
            $_POST[$clave] = strip_tags($_POST[$clave]);
            if(is_array($_POST[$clave])) 
                return array_map(__METHOD__, $_POST[$clave]); 

            if(!empty($_POST[$clave]) && is_string($_POST[$clave])) { 
                return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $_POST[$clave]); 
            } 

            return trim($_POST[$clave]);
        }
    } 
    
    /* Valida un email */
    public function validarEmail($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return false;
        }
        return true;
    }

    
    public function validarFormatoValido($type){
        $admitidos = array('image/gif', 'image/jpeg', 'image/png');
        
        if(in_array($type, $admitidos))
            return true;
        return false;
    }
    
    public function retornarFormato($type){
        switch($type){
            case 'image/gif':
                $data = 'gif';
                break;
            case 'image/jpeg':
                $data = 'jpg';
                break;
            case 'image/png':
                $data =  'png';
                break;
            default:
                $data = false;
        }
        return $data;
    }
    
    public function generarCodigoPedido(){
        return substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5)),14,15);
    }
    
    protected function redireccionar($ruta = false){
        if($ruta){
            header('location:' . BASE_URL . $ruta);
            exit;
        }
        else{
            header('location:' . BASE_URL);
            exit;
        }
    }
}
