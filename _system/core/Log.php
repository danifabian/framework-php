<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Description of Session
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Log {
    
    
    public static function myLog($msg){
        $debug = FirePHP::getInstance(true);
        $debug->log("Entra myLog");
        error_log($msg, 3, LOGS_PATH);
    }
}
