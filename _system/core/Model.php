<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Description of Model
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Model {
    private $_registry;
    protected $_db;
    
    public function __construct() {
        $this->_registry = Registry::getInstancia();
        $this->_db = $this->_registry->_db;
    }
}
