<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Gestiona las peticiones recibidas por la URL
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Request {
    
    private $_controlador;
    private $_metodo;
    private $_args;
    
    public function __construct() {
        //echo $_GET['url'];
        if(isset($_GET['url'])){
            $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
            
            $url = explode('/', $url);
            $url = array_filter($url);
            
            $this->_controlador = strtolower(array_shift($url));
            $this->_metodo = strtolower(array_shift($url));
            $this->_args = $url;
        }
        
        if(!$this->_controlador){
            $this->_controlador = DEFAULT_CONTROLLER;
        }
        if(!$this->_metodo){
            $this->_metodo = 'index';
        }
        if(!isset($this->_args)){
            $this->_args = array();
        }
    }
    
    /*
     * Retornar el controlador enviado a traves de la URL
     */
    public function getControlador(){
        return $this->_controlador;
    }
    /*
     * Retornar el método enviado a traves de la URL
     */
    public function getMetodo(){
        return $this->_metodo;
    }
    /*
     * Retornar los argumentos enviados a traves de la URL
     */
    public function getArgs(){
        return $this->_args;
    }
}
