<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Description of View
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class View {
    private $_controlador;
    private $_metodo;
    
    public function __construct(Request $peticion) {
        $this->_controlador = $peticion->getControlador();
        $this->_metodo = $peticion->getMetodo();
    }
    
    public function render($vista, $item = false){
        $rutaView = APPPATH . 'views' . DS . $this->_controlador . DS . $vista . '.phtml';
        $userMenu =  $this->_getUserMenu();
        
        $_layoutParams = array(
            'layout_css' => BASE_URL . 'app/_layout/' . DEFAULT_LAYOUT . '/css/',
            'layout_js' => BASE_URL . 'app/_layout/' . DEFAULT_LAYOUT . '/js/',
            'layout_img' => BASE_URL . 'app/_layout/' . DEFAULT_LAYOUT . '/img/',
            'public_css' => BASE_URL . 'public/css/',
            'public_js' => BASE_URL . 'public/js/',
            'public_img' => BASE_URL . 'public/img/',
            'menu' => $userMenu,
            'sesion_menu' => $this->_getSesionMenu(),
            'admin_menu' => $this->_adminMenu(),
        );
        
        
        //No mostrar cabecera y pié de página por defecto para la página 
        //de inicio de sesión de backend. Esta página sera una página que tendrá
        //su propio estilo
        $this->adminPanel = array('adminpanel', 'adminclientes', 'adminproductos');
        if($this->_controlador == 'backend' || ($this->_controlador == 'carrito' 
                                                && $this->_metodo == 'init' 
                                                && $this->_metodo == 'addItem'
                                                && $this->_metodo == 'deleteItem')){
            if(is_readable($rutaView)){
                include_once $rutaView;
            }
        }
        else if(is_readable($rutaView)){
            include_once LAYOUT_PATH . 'header.php';
            include_once $rutaView;
            include_once LAYOUT_PATH . 'footer.php';
        }
        else{
            //header('location:' . BASE_URL . 'err/acceso/404'); //Descomentar cuando este en producción
            throw new Exception('ERROR: No existe la vista en ' . $rutaView);
        }
    }
    
    /**
     * 
     * @return array
     */
    private function _getUserMenu(){
        $menu = array(
            array(
                'id' => 'inicio',
                'titulo' => 'Inicio',
                'enlace' => BASE_URL,
                'img_name' => 'home.png'
            ),
            array(
                'id' => 'noticias',
                'titulo' => 'Notícias',
                'enlace' => BASE_URL .'noticias',
                'img_name' => 'news.png'
            ),
            array(
                'id' => 'skateboard',
                'titulo' => 'Skateboard',
                'enlace' => BASE_URL .'skateboard',
                'img_name' => 'skateboard.png',
                array(
                    array(
                    'id' => 'skate-completo',
                    'titulo' => 'Completos',
                    'enlace' => BASE_URL .'completoboard',
                    'img_name' => 'skateboard.png'
                    ),
                    array(
                        'id' => 'skate-table',
                        'titulo' => 'Tablas',
                        'enlace' => BASE_URL .'tablasboard',
                        'img_name' => 'skateboard.png'
                    ),
                    array(
                        'id' => 'skate-ejes',
                        'titulo' => 'Ejes',
                        'enlace' => BASE_URL .'ejesboard',
                        'img_name' => 'ejes.png'
                    ),
                    array(
                        'id' => 'skate-ruedas',
                        'titulo' => 'Ruedas',
                        'enlace' => BASE_URL .'ruedasboard',
                        'img_name' => 'ruedas.png'
                    ),
                    array(
                        'id' => 'skate-cojinetes',
                        'titulo' => 'Cojinetes',
                        'enlace' => BASE_URL .'cojinetes',
                        'img_name' => 'cojinetes.png'
                    )
                )
            ),
            array(
                'id' => 'longboard',
                'titulo' => 'Longboard',
                'enlace' => BASE_URL .'longboard',
                'img_name' => 'longboard.png',
                array(
                    array(
                    'id' => 'long-completo',
                    'titulo' => 'Completos',
                    'enlace' => BASE_URL .'completolong',
                    'img_name' => 'longboard.png'
                    ),
                    array(
                        'id' => 'long-table',
                        'titulo' => 'Tablas',
                        'enlace' => BASE_URL .'tablaslong',
                        'img_name' => 'longboard.png'
                    ),
                    array(
                        'id' => 'long-ejes',
                        'titulo' => 'Ejes',
                        'enlace' => BASE_URL .'ejeslong',
                        'img_name' => 'ejes.png'
                    ),
                    array(
                        'id' => 'long-ruedas',
                        'titulo' => 'Ruedas',
                        'enlace' => BASE_URL .'ruedaslong',
                        'img_name' => 'ruedas.png'
                    ),
                    array(
                        'id' => 'long-cojinetes',
                        'titulo' => 'Cojinetes',
                        'enlace' => BASE_URL .'cojinetes',
                        'img_name' => 'cojinetes.png'
                    )
                )
            ),
            array(
                'id' => 'chico',
                'titulo' => 'Chico',
                'enlace' => BASE_URL .'chico',
                'img_name' => 'chico.png'
            ),
            array(
                'id' => 'chica',
                'titulo' => 'Chica',
                'enlace' => BASE_URL .'chica',
                'img_name' => 'chica.png'
            ),
            array(
                'id' => 'accesorio',
                'titulo' => 'Accesorios',
                'enlace' => BASE_URL .'accesorio',
                'img_name' => 'accesorios.png',
                array(
                    array(
                        'id' => 'accesorios-lija',
                        'titulo' => 'Lija',
                        'enlace' => BASE_URL .'lija',
                        'img_name' => 'lija.png'
                    ),
                    array(
                        'id' => 'accesorios-gomas',
                        'titulo' => 'Gomas',
                        'enlace' => BASE_URL .'goma',
                        'img_name' => 'gomas.png'
                    ),
                    array(
                        'id' => 'accesorios-tornillos',
                        'titulo' => 'Tornillos',
                        'enlace' => BASE_URL .'tornillos',
                        'img_name' => 'tornillos.png'
                    ),
                    array(
                        'id' => 'accesorios-herramientas',
                        'titulo' => 'Herramientas',
                        'enlace' => BASE_URL .'herramientas',
                        'img_name' => 'herramientas.png'
                    ),
                    array(
                        'id' => 'accesorios-protecciones',
                        'titulo' => 'Protecciones',
                        'enlace' => BASE_URL .'protecciones',
                        'img_name' => 'protecciones.png'
                    ),
                    array(
                        'id' => 'accesorios-cordones',
                        'titulo' => 'Cordones',
                        'enlace' => BASE_URL .'cordones',
                        'img_name' => 'cordones.png'
                    )
                )
            ),
            array(
                'id' => 'accesorios-marcas',
                'titulo' => 'Marcas',
                'enlace' => BASE_URL .'marcas',
                'img_name' => 'brand.png'
            )
        );
        return $menu;
    }
    
    
    private function _getSesionMenu(){
        $sesionMenu = array(
            'id' => Session::get('username'),
            'titulo' => Session::get('username'),
            //'enlace' => '',
            'img_name' => 'ic-account',
            array(
                'id' => 'mostrar-perfil',
                'titulo' => 'Ver perfil',
                'enlace' => BASE_URL . 'perfil',
                'img_name' => 'ic-update'
            ),
            array(
                'id' => 'admin-panel',
                'titulo' => 'Panel administrador',
                'enlace' => BASE_URL . 'adminpanel',
                'img_name' => 'ic-panel'
            ),
            array(
                'id' => 'close-sesion',
                'titulo' => 'Cerrar sesión',
                'enlace' => BASE_URL . 'usuarios/cerrar_sesion',
                'img_name' => 'ic-account'
            )
        );
        return $sesionMenu;
    }
    
    private function _adminMenu(){
        $admin = array(
            array(
                'id' => 'inicio',
                'titulo' => 'Inicio / Mostar notícias',
                'enlace' => BASE_URL . 'adminpanel',
                'img_name' => 'news.png'
            ),
            array(
                'id' => 'productos',
                'titulo' => 'Productos',
                'enlace' => BASE_URL .'adminproductos',
                'img_name' => 'skateboard.png',
            ),
            array(
                'id' => 'clientes',
                'titulo' => 'clientes',
                'enlace' => BASE_URL .'adminclientes',
                'img_name' => 'ic_face.png',
            ),
            array(
                'id' => 'tienda',
                'titulo' => 'Volver a la tienda',
                'enlace' => BASE_URL ,
                'img_name' => 'ic_store_mall.png'
            )
        );
        
        return $admin;
    }
}
