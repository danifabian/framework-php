<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Gestiona la aplicacion. Se encarga principalmente
 * de llamar a los controladores, metodos y enviarle los
 * parametros recibidos por la url.
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Bootstrap {
    /**
     * run
     * @param Request $peticion Contiene la url divida por controlador, método y argumentos
     */
    public static function run(Request $peticion){
        $controller = $peticion->getControlador() . 'Controller';
        $rutaControlador = APPPATH . 'controllers' . DS . $controller . '.php';
        $metodo = $peticion->getMetodo();
        $args = $peticion->getArgs();
        //Saber si la ruta es accesible
        if(is_readable($rutaControlador)){
            require_once $rutaControlador;
            $controller = new $controller;
            
            if(is_callable(array($controller, $metodo))){
                $metodo = $peticion->getMetodo();
            }
            else{
                $metodo = 'index';
            }
            if($args != NULL){//Comprobar que el array de argumentos no este vacio
                call_user_func_array(array($controller, $metodo), $args);
            }
            else{
                call_user_func(array($controller, $metodo));
            }
        }
        else{
            header('location:' . BASE_URL . 'err/acceso/404');
            //throw new Exception('ERROR: Controlador no encontroado<br>');
        }
    }
}
