<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Description of Session
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Session {
    
    public static function init(){
        session_start();
    }
    
	/**
     * Destruir una o varias variables de sesion
	 */
    public static function destroy($key = false){
        if($key){
            if(is_array($key)){
                for($i = 0; $i < count($key); $i++){
                    if(isset($_SESSION[$key])){
                        unset($_SESSION[$key]);
                    }
                }
            }
            else{
                if(isset($_SESSION[$key])){
                    unset($_SESSION[$key]);
                }
            }
        }
        else{
            session_destroy();
        }
    }
    
    /**
     * Añadir una variable de sesión
     * @param type $key Nombre de parametro de sesión
     * @param type $value Valor a recibir el parámetro de sesión
     */
    public static function set($key, $value){
        if(!empty($key)){//verificacion de existencia
            $_SESSION[$key] = $value;
        }
    }
    
    /**
     * Devolver una variable de sesión.
     * @param type $key
     * @return type
     */
    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }    
    }
    
    /**
     * 
     * @param type $level Nivel de usuario mínimo/requerido para tener permiso
     */
    public static function acceso($level){
        if(!Session::get('userAuth')){//Si no hay session
            header('location:' . BASE_URL . 'err/acceso/503');
            exit;
        }
        
        Session::sessionTime();
        
        //Comprobar si el usuario con acceso level tiene permisos
        if(Session::getLevel($level) > Session::getLevel(Session::get('level'))){
            header('location:' . BASE_URL . 'err/acceso/503');
            exit;
        }
        
    }
    
    /**
     * Permite mostrar/ocultar contenido de la vista segun permiso de sesión del usuario
     * @param type $level
     * @return boolean
     */
    public static function accesoVista($level){
        if(!Session::get('userAuth')){//Si no hay session
            return false;
        }
        
        if(Session::getLevel($level) > Session::getLevel(Session::get('level'))){
            return false;
        }
        //Solo si pasa las validadciones
        return true;
    }
    
    /**
     * Colocar los diferentes niveles de acceso que hay en nuestra app
     * @param type $level
     */
    public static function getLevel($level){
        $role['admin'] = 3;
        $role['gestor'] = 2;
        $role['usuario'] = 1;
        
        if(!array_key_exists($level, $role)){
            throw new Exception('ERROR: No existe level en sesion o parametro incorreto');
        }
        else{
            return $role[$level];
        }
    }
    
    /**
     * Permite seleccionar grupos de usuarios especificos para asignarles permiso
     */
    public static function accesoEstricto(array $level, $noAdmin = false) {
        if(!Session::get('userAuth')){
            header('location:' . BASE_URL . 'err/acceso/503');
            exit;
        }
        
        if($noAdmin == false){
            if(Session::get('level') == 'admin'){
                return;
            }
        }
        
        if(count($level)){
            if(in_array(Session::get('level'), $level)){
                return;
            }
        }
        header('location:' . BASE_URL . 'err/acceso/401');
    }
    
    public static function accesoViewEstricto(array $level, $noAdmin = false){
        if(!Session::get('userAuth')){
            return false;
        }
        
        if($noAdmin == false){
            if(Session::get('level') == 'admin'){
                return true;
            }
        }
        
        if(count($level)){
            if(in_array(Session::get('level'), $level)){
                return true;
            }
        }
        
        return false;
    }
    
    
    public static function sessionTime(){
        if(!Session::get('tiempo') || !defined('SESSION_TIME')){
            throw new Exception('No se ha definido el tiempo de sesion'); 
        }
        
        if(SESSION_TIME == 0){
            return;
        }
        $debug = FirePHP::getInstance(true);
        $debug->log(Session::get('tiempo'), "Sesion tiempo");
        if(time() - Session::get('tiempo') > (SESSION_TIME * 60)){
            
            Session::destroy();
            header('location:' . BASE_URL . 'err/acceso/408');
        }
        else{
            Session::set('tiempo', time());
        }
    }
}


/**
 * Final Session.php
 * Location: _system/core/Session.php
 */
