<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Utilizar el patron singleton para verificar que solo se realiza una instancia
 * de una clase
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Registry {
    private static $_instancia;//Instancia del registro
    private $_data;
    
    //No se puede crear una instancia de la clase
    //Solo se puede instanciar desde la propia clase
    private function __construct() {
        
    }
    
    //Aqui empieza el patron Singleton
    public static function getInstancia(){
        //Si no tiene una instancia del registro creada se crea
        if(!self::$_instancia instanceof self){
            self::$_instancia = new Registry();
        }
        return self::$_instancia;
    }
    
    //Guardar objetos en el registro
    public function __set($name, $value) {
        $this->_data[$name] = $value;
    }
    //Devolver objetos
    public function __get($name) {
        if(isset($this->_data[$name])){
            return $this->_data[$name];
        }
        return false;
    }
}

