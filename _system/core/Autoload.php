<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Auto cargar clases del sistema segun se vayan instanciando
 * @param type $class Clase instanciada
 */
function autoloadCore($class){
    $file = CORE_PATH . ucfirst(strtolower($class)) . '.php';
    if(file_exists($file)){
        include_once $file;
    }
}

/*
 * Si se requiere cargar otros modulos añadir aqui la funcion
 */

spl_autoload_register('autoloadCore');

/*
 * Añadir spl_autoload_register 
 */

