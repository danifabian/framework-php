<?php
defined('ROOT') OR exit('No direct script access allowed');

/**
 * Indicar ruta base
 * 
 * $base_url = 'localhost/myApp';
 */
$base_url = $_SERVER['HTTP_HOST'];
//$base_url = "deic-dc0.uab.cat/~tdiw-d7";

/**
 * Gestión de aplicaciones (en caso de tener más de una)
 * 
 * $app_path = 'Applicacion';
 */
$app_path = 'SkateShop';
/**
 * Elegir layout 
 * Ver en _app/_layout los distintos tipos de layout disponibles
 * (totalmente configurable)
 */
$default_layout = 'default';

/**
 * Configuración de BD
 * $db_host = 'localhost';
 * $db_name = 'prod_skate_shop';
 * $db_user = 'root';
 * $db_password = 'kinshasa';
 * $db_charset = 'utf8';
 */
$db_host = 'localhost';
$db_name = 'prod_skate_shop';
$db_user = 'root';
$db_password = 'kinshasa';
/*$db_name = 'tdiw-d7';
$db_user = 'tdiw-d7';
$db_password = 'tdiw-d7-A8';*/
$db_charset = 'utf8';

/**
 * Configurar tiempos de sesion
 * $session_time = minutos;
 */
$session_time = 20;/* 1 minuto (pruebas) */

/**
 * Configurar seguridad encriptacion
 * Nota: Una vez especificada una key y se trabaje con ella,
 * no modificar o los usuarios perderan su acceso
 * $hash_key = '1234567890abcdefghijklmnopkrstuvwxyz';
 * $algoritmo_hash = 'sha1';
 */
$hash_key = '4fa276bbcfa5bg#|gdsac@oÑPbm';
$algoritmo_hash = 'sha1';

//Configuracion rutas
/*
 * Deben seguir el formato 'dir1' . DS . 'subdir1' . DS . 'etcetc';
 */
//Operativo en la siguiente versin del framework
/*$log_path = 'tmp' . DS . 'logs';
$log_name = 'error_log.txt';
$cache_path = 'tmp' . DS . 'cache';*/

/*
 * >Environment
 * Configuracion: 
 */
/**
 * Final Config.php
 * Location: _system/core/Config.php
 */
