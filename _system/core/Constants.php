<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Constantes del framework
 */

/*
 * Saber protocolo de conexión (HTTP / HTTPS)
 */
$protocolo = \filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
$protocolo = \explode('/', $protocolo);
$protocolo = \array_filter($protocolo);
$protocolo = strtolower(array_shift($protocolo));

/*
 * Configuración de rutas
 */
//URL base
define('BASE_URL' , $protocolo . '://' . $base_url . '/');
//Ruta de aplicación cargada
define('APPPATH', ROOT . 'app' . DS . $app_path . DS);
define('LAYOUT_PATH', ROOT . 'app' . DS . '_layout' . DS . $default_layout . DS);
//Ruta logs
/*define('LOGS_PATH', ROOT . $log_path . DS . $log_name);
define('CACHE_PATH', ROOT . $cache_path . DS);*/
//Ruta cache
/*
 * Nombres
 */
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', $default_layout);
define('APP_RINNUNG', $app_path);
define('APPNAME', 'Dafaos');
define('APPVERSION', '1.0');
/*
 * BD Constants
 */
define('DB_HOST', $db_host);
define('DB_NAME', $db_name);
define('DB_USER', $db_user);
define('DB_PASSWORD', $db_password);
define('DB_CHARSET', $db_charset);
/*
 * Session time
 */
define('SESSION_TIME', $session_time);
/* Seguridad */
define('HASH_KEY', $hash_key);
define('HASH_ALGORITMO', $algoritmo_hash);
