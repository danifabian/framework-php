<?php
defined('ROOT') OR exit('No direct script access allowed');
/**
 * Description of Database
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class Database extends PDO{
    
    public function __construct() {
        $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
        parent::__construct($dsn, 
                DB_USER, 
                DB_PASSWORD, 
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHARSET));
        
        //Cambia atributo por defecto del metodo fecth
        //Para más ayuda revisar: 
        //Método setAttribute: http://php.net/manual/es/pdo.setattribute.php
        //Constantes PDO: http://php.net/manual/es/pdostatement.fetch.php
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
    
}
