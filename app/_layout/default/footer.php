             <!-- Fin seccion contenido -->
            </section>
            
            <!-- Pié de página -->
            <footer class="footer">
                <div class="general-info">
                    <div>
                        <ul>
                            <li>Gui de compras</li>                                
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/envio">Método de envio</a></li>                                
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/compra">Método de compra</a></li>
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/cambios">Cambios / devoluciones</a></li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Información general</li>
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/contacto">Contacto</a></li>
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/sobrenosotros">Sobre nosotros</a></li>
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/dondeestamos">Dónde estamos</a></li>
                            <li><a href="<?php echo BASE_URL; ?>infogeneral/politicapriv">Política de privacidad</a></li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Nuestro compromiso</li>
                            <li><span><img src="<?php echo $_layoutParams['layout_img'] ?>footer/ic_store_mall.png" alt="" class="ico-small" /></span>Disponibilidad de stock</li>
                            <li><span><img src="<?php echo $_layoutParams['layout_img'] ?>footer/ic_transport.png" alt="" class="ico-small" /></span>Envios en 24/72 horas</li>
                            <li><span><img src="<?php echo $_layoutParams['layout_img'] ?>footer/ic_sync.png" alt="" class="ico-small" /></span>Facilidad de cambios y devoluciones</li>
                            <li><span><img src="<?php echo $_layoutParams['layout_img'] ?>footer/ic_check.png" alt="" class="ico-small" /></span>Satisfacción de los clientes</li>
                        </ul>
                    </div>
                </div>
                <div class="">
                    <p>
                        <a href="http://jigsaw.w3.org/css-validator/check/referer">
                            <img style="border:0;width:88px;height:31px"
                                src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
                                alt="¡CSS Válido!" />
                        </a>
                    </p>
                    <p>Copyright &copy; 2015 - <?php echo APPNAME ?> - version <?php echo APPVERSION; ?></p>
                </div>
                <div class="politica_cookies">
                    <p>Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. 
                        Si continía navegando consideramos que acepta el uso de cookies. <br />
                        Encontrará más información en nuestra <a href="<?php echo BASE_URL; ?>infogeneral/politicaCookies">Política de cookies</a></p>
                    <a href="javascript:void(0);" class="acetpa_politica_cookies"><b>OK</b></a> | 
                    <a href="http://politicadecookies.com" target="_blank" class="info">Más información</a>
                </div>
            </footer>
        </div><!-- End div main wrapper -->
        
        <!-- Mensaje indicando si las cookies estan habilitadas -->
        <div class="c_enabled">
            <p>No tiene las cookies habilitadas en su navegador. <br />
            Por favor, habilítelas para navegar en la página.<br />
            Gracias por las molestias.
            </p>
        </div>
    </body>
</html>
