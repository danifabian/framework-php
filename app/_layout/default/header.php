<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $_layoutParams['layout_img']; ?>favicon.ico">
        <title><?php echo $this->titulo; ?></title>
        <meta charset="UTF-8">
        <meta name="author" content="Dani Fabián">
        <meta name="author" content="Sergio Gallo">
        <meta name="keywords" content="Skateshop, skateboard, skate, longboar, long, tablas, ruedas, ropa, chica, chico">
        <meta name="description" content="Skateshop! La mejor tienda del momento para realizar tus compras online">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<?php echo $_layoutParams['layout_css']; ?>layout.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $_layoutParams['public_css']; ?>style.css" />
        <script src="<?php echo $_layoutParams['public_js']; ?>jquery-1.11.3.js" type="text/javascript"></script>
        <script src="<?php echo $_layoutParams['public_js']; ?>jquery-ui.js" type="text/javascript"></script>
        <script src="<?php echo $_layoutParams['public_js']; ?>formulario.js" type="text/javascript"></script>
        <script src="<?php echo $_layoutParams['public_js']; ?>jquery_functions.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- Contenedor para facilitar uso de propiedad flex -->
        <div class="wrapper">
            <!-- Cabecera -->
            <header class="header">
                <div class="header-first">
                    <h1>No hay principio o fin, sólo adrenalina!</h1>
                    <figure>
                        <a href="<?php echo BASE_URL; ?>"><img src="<?php echo $_layoutParams['layout_img']; ?>header/logo.png" alt="logo" /></a>
                    </figure>
                </div>
                <hr /><!-- Opcional -->
                <div class="header-second">
                    <div>
                        <a href="<?php echo BASE_URL; ?>carrito/mostrarCarrito">Carrito<span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_shopping_cart.png" alt="ic_shopping" class="ico-small" /></span></a>
                        <div class="cart-list-wrapper"><!-- Contenedor carrito -->
                            <div class="item-list"><!-- Listado de items del carrito -->
                                
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>Total items: <span class="total-items"></span></td>
                                        <td>Precio total: <span class="total-price"></span> €</td>
                                    </tr>
                                </table>
                                <!-- Despues de esta tabla deberia insertarse con jquery dos botone,
                                uno para enviar al formulario de compra y otro para limpiar el carrito-->
                            </div>
                        </div>
                    </div>
                    <div>
                    <?php if(Session::get('userAuth')): ?>
                        <ul class="session-menu">
                            <li>
                                <?php echo $_layoutParams['sesion_menu']['titulo']; ?>
                                <span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_account_circle.png" alt="ic_account" class="ico-small" /></span>
                                <ul class="session-submenu">
                                    <li>
                                        <a href="<?php echo $_layoutParams['sesion_menu'][0]['enlace']; ?>">
                                            <?php echo $_layoutParams['sesion_menu'][0]['titulo']; ?>
                                            <span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_settings.png" alt="ic_settings" class="ico-small" /></span>
                                        </a>
                                    </li>
                                    <?php if(Session::accesoVista('gestor')): ?>
                                    <li>
                                        <a href="<?php echo $_layoutParams['sesion_menu'][1]['enlace']; ?>">
                                            <?php echo $_layoutParams['sesion_menu'][1]['titulo']; ?>
                                            <span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_dashboard.png" alt="ic_adminpanel" class="ico-small" /></span>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <li>
                                        <a href="<?php echo $_layoutParams['sesion_menu'][2]['enlace']; ?>">
                                            <?php echo $_layoutParams['sesion_menu'][2]['titulo']; ?>
                                            <span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_close.png" alt="ic_close" class="ico-small" /></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    <?php else: ?>                        
                        <a href="<?php echo BASE_URL; ?>usuarios">Identifícate<span><img src="<?php echo $_layoutParams['layout_img']; ?>header/ic_account_circle.png" alt="ic_account" class="ico-small" /></span></a>
                    <?php endif; ?>
                    </div>
                </div>
            </header>
            <!-- Contenido opcional -->
            <aside class="aside aside-1">
                <form action="<?php echo BASE_URL; ?>buscador/index" method="post" id="searcher">
                    <input type="search" value="" name="buscador" placeholder="Buscador por marcas" />
                    <button type="submit" name="searcher-button" value="Buscar">
                        <img class="ico-small" alt="ico-lupa" src="<?php echo $_layoutParams['layout_img']; ?>searcher/ic_search.png" /><!-- Contiene icono lupa para l buscador -->
                    </button>
                </form>
                
                <nav>
                    <span class="menu-ico"><img class="ico-large" src="<?php echo $_layoutParams['layout_img']; ?>menu/ic_menu_48.png" alt="hamburguer-ico"/></span>
                    
                    <?php if(!in_array($this->_controlador, $this->adminPanel)): ?>
                    <ul id="main-menu">
                        <?php if(isset($_layoutParams['menu'])): ?>
                        <?php foreach($_layoutParams['menu'] as $menu): ?>
                            <?php 
                            if($item && $menu['id'] == $item ){ 
                                $_item_style = 'current'; 
                            } else {
                                $_item_style = '';
                            }
                            ?>
                            <li>
                                <a href="<?php echo $menu['enlace']; ?>" class="<?php echo $_item_style; ?>">
                                    <img src="<?php echo $_layoutParams['layout_img']; ?>menu/<?php echo $menu['img_name']; ?>" 
                                         alt="<?php echo $menu['img_name']; ?>" 
                                         class="ico-small" />
                                        <?php echo $menu['titulo']; ?>
                                </a>
                                
                                <?php if(array_key_exists(0, $menu)): ?>
                                <ul class="sub-menu">
                                    <?php foreach ($menu[0] as $submenu): ?>
                                        <?php 
                                        if($item && $submenu['id'] == $item ){ 
                                            $_item_style = 'current'; 
                                        } else {
                                            $_item_style = '';
                                        }
                                        ?>
                                    <li>
                                        <a href="<?php echo $submenu['enlace']; ?>" class="<?php echo $_item_style; ?>">
                                            <img src="<?php echo $_layoutParams['layout_img']; ?>menu/<?php echo $submenu['img_name']; ?>" 
                                                 alt="<?php echo $submenu['id']; ?>" 
                                                 class="ico-small" />
                                                <?php echo $submenu['titulo']; ?>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                    <?php else: ?>
                        <?php if(Session::accesoVista('gestor')): ?>
                        <ul id="main-menu">
                            <?php if(isset($_layoutParams['admin_menu'])): ?>
                                <?php foreach($_layoutParams['admin_menu'] as $menu): ?>
                                    <?php 
                                    if($item && $menu['id'] == $item ){ 
                                        $_item_style = 'current'; 
                                    } else {
                                        $_item_style = '';
                                    }
                                    ?>
                                    <li>
                                        <a href="<?php echo $menu['enlace']; ?>" class="<?php echo $_item_style; ?>">
                                            <img src="<?php echo $_layoutParams['layout_img']; ?>menu/<?php echo $menu['img_name']; ?>" 
                                                 alt="<?php echo $menu['img_name']; ?>" 
                                                 class="ico-small" />
                                                <?php echo $menu['titulo']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                        <?php endif; ?>
                    <?php endif; ?>
                </nav>
            </aside>
            
            <!-- Contenido -->
            <section class="main">
               
                <header class="hook">
                    <h5><?php echo $this->hook; ?></h5>
                    <?php if(in_array($this->_controlador, $this->adminPanel)): ?>
                        <?php switch($this->_controlador): 
                        case 'adminpanel': ?>
                        <span class="create-new-admin-element">
                            <a href="<?php echo BASE_URL; ?>adminpanel/nuevaNoticia">Nueva notícia</a>
                        </span>
                        <?php break; 
                        case 'adminproductos': ?>
                        <span class="create-new-admin-element">
                            <a href="<?php echo BASE_URL; ?>adminproductos/nuevoProducto">Nuevo producto</a>
                        </span>
                        <?php break; 
                        endswitch;?>
                    <?php endif; ?>
                </header>
                <?php //endif; ?>
                <div id="notifications">
                    <div class="button-warning"></div>
                    <div class="button-error"></div>
                    <div class="button-success"></div>
                </div>
                