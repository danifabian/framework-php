<?php

class completoboardController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $completoboard = $this->loadModel("completoboard");
        #$this->_view->tablas = $completoboard->getCompletoBoard();
        $this->_view->tablas = $paginador->paginar($completoboard->getCompletoBoard(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'completoboard/index');
        $this->_view->titulo = "Tablas Completas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Tablas Completas';
        $this->_view->render('index', 'skateboard/completos');
    }
       
     public function verProducto($id){
        $skate = $this->loadModel("completoboard");
        $this->_view->skate = $skate->getTablaCompleta($this->filtrarInt($id));
        $this->_view->titulo = "Tabla Completa";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >><a href="'. BASE_URL . 'completoboard">Completa</a> >> Tabla Completa';
        $this->_view->render('verProducto', 'completos');
    }
}
