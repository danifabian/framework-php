<?php

class accesorioController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $acce = $this->loadModel("accesorios");
        $this->_view->acce = $paginador->paginar($acce->getAccesorios(), $pagina);
        #$this->_view->acce = $acce->getAccesorios();
        $this->_view->paginacion = $paginador->getView('paginador', 'accesorio/index');
        $this->_view->titulo = "Accesorios";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios';
        $this->_view->render('index', 'accesorios');
    }
    
    public function verProducto($id){
        $acce = $this->loadModel("accesorios");
        $this->_view->acce = $acce->getAccesorio($this->filtrarInt($id));
        $this->_view->acce = array_pop($this->_view->acce);
        $this->_view->titulo = $this->_view->acce['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'accesorios">Accesorios</a> >> ' . $this->_view->acce['modelo'];
        $this->_view->render('verProducto', 'accesorio');
    }
}