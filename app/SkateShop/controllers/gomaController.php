<?php

class gomaController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $goma = $this->loadModel("goma");
        #$this->_view->goma = $goma->getGomas();
        $this->_view->goma = $paginador->paginar($goma->getGomas(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'goma/index');
        $this->_view->titulo = "Accesorios Gomas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios Gomas';
        $this->_view->render('index', 'accesorios/goma/');
    }
    
    public function verProducto($id){
        $goma = $this->loadModel("goma");
        $this->_view->goma = $goma->getGoma($this->filtrarInt($id));
        $this->_view->goma = array_pop($this->_view->goma);
        $this->_view->titulo = $this->_view->goma['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'goma">Gomas</a> >> ' . $this->_view->goma['modelo'];
        $this->_view->render('verProducto', 'goma');
    }
}