<?php
/**
 * Description of erroController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class errController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        $this->_view->titulo = 'Error';
        $this->_view->hook = 'Error';
        $this->_view->mensaje = $this->_getError();
        $this->_view->render("index");
    }
    
    public function acceso($key){
        $this->_view->key = $key;
        $this->_view->titulo = 'Error';
        $this->_view->hook = 'Error';
        $this->_view->mensaje = $this->_getError($key);
        $this->_view->render("acceso");
    }
    
    private function _getError($errCode = false){
        if($errCode){
            $errCode = $this->filtrarInt($errCode);
            if(is_int($errCode)){
                $errCode = $errCode;
            }
        }
        else{
            $errCode = 'default';
        }        
        
        $error['default'] = 'Ha ocurrido un error y la página no puede mostrarse';
        $error['401'] = 'No está autorizado para entrar en este área!';
        $error['404'] = 'Página no encontrada!';
        $error['408'] = 'Tiempo de sesión agotado.<br />Por favor, inicie sesión';
	$error['503'] = 'Acceso restringido!';
	$error['4848'] = 'Error con directorio de subida o el directorio no tiene permisos de escritura!';
        
        if(array_key_exists($errCode, $error)){
            return $error[$errCode];
        }
        else{
            return $error['default'];
        }
    }
}
