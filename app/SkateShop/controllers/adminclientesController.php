<?php

class adminclientesController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false) {
        Session::acceso('gestor');
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $clie = $this->loadModel("adminclientes");
        $this->_view->clie = $paginador->paginar($clie->getClientes(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'adminclientes/index');
        //$this->_view->clie = $clie->getClientes();
        $this->_view->titulo = "Administración de clientes";
        $this->_view->hook = '<a href="'.BASE_URL.'adminpanel">Inicio</a> >> Administracción de clientes';
        $this->_view->render('index', 'adminclientes');
    }
    
    public function verFacturas($id){
        Session::acceso('gestor');
        $clie = $this->loadModel("facturas");
        $this->_view->clie = $clie->getFacturas($id);
        $this->_view->titulo = 'Facturas';
        $this->_view->hook = '<a href="'.BASE_URL.'adminpanel">Inicio</a> >> <a href="' . BASE_URL . 'adminclientes">Administración de clientes</a> >> Facturas del cliente: '.$id.'';
        $this->_view->render('verfacturas', 'adminclientes');
    }
    public function verFactura($id){
        Session::acceso('gestor');
        $clie = $this->loadModel("adminclientes");
        $this->_view->clie = $clie->getFactura($id);
        $this->_view->titulo = 'Facturas';
        $this->_view->hook = '<a href="'.BASE_URL.'adminpanel">Inicio</a> >> <a href="' . BASE_URL . 'adminclientes">Administración de clientes</a> >> Facturas del cliente: '.$id.'';
        $this->_view->render('verfacturas', 'adminclientes');
    }
}
