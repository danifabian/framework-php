<?php
/**
 * Description of carritoController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class carritoController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    public function index(){}

    public function mostrarCarrito(){
        $data = array();
        foreach(Session::get('carrito') as $item){
            $data[$item['id']] = $item['cantidad']*$item['precio'];
        }
        
        $this->_view->carrito = Session::get('carrito');
        $this->_view->totalPrice = $data;
        $this->_view->titulo = 'Titulo';
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Su carrito ' . Session::get('username');
        $this->_view->render('index');
    }
    
    public function eliminarItem($id){
        $carrito = Session::get('carrito');
            
        if($this->_checkItem($id)){
            unset($carrito[$id]);
            Session::set('carrito', $carrito);
        }
        $this->redireccionar('carrito/mostrarCarrito');
    }
    
    public function editarItem($id){
        $this->_view->titulo = 'Editar elemento';
        $this->_view->hook = 'Editar cantidad de items';

        //Comprobar que realmente existe ese elemento a editar en el carrito y 
        //No lo a modificado el usuario por malicia
        if(array_key_exists($this->filtrarInt($id), Session::get('carrito'))){
            $carrito =  Session::get('carrito');
            $carrito = array_values($carrito[$this->filtrarInt($id)]);
            $this->_view->item = $carrito;
        }
        else{
            echo 'No tienes ese producto en su lista';
            $this->_view->_error = 'No tienes ese producto en su lista';
        }
        
        $this->_view->render('editarItems');
    }
    
    public function editarAccion(){
        if($this->getPostParam('btn-submit')){
            $carrito = Session::get('carrito');
            if(array_key_exists($this->getPostInt('data'), $carrito)){
                if($this->getPostInt('cantidad') > 0){
                    $carrito[$this->getPostInt('data')]['cantidad'] = $this->getPostInt('cantidad');
                    Session::set('carrito', $carrito);
                }
                else{
                    unset($carrito[$this->getPostInt('data')]);
                    Session::set('carrito', $carrito);
                }
            }
        }
        $this->redireccionar('carrito/mostrarCarrito');
    }
    
    /* Desde aqui se utiliza para las transacciones mediante AJAX */
    public function init(){
        $carrito = array();
        header("Content-Type: text/json");
        if(!Session::get('carrito')){
            Session::set('carrito', $carrito);
        }
        echo json_encode(Session::get('carrito'));
    }

    //$debug = FirePHP::getInstance(true);
    //$debug->log(Session::get('carrito'), "Carrito");

    public function addItem(){
        $carrito = array();
        header("Content-Type: text/json");
        
        if($_POST['json']){
            $json = json_decode($_POST['json']);
            $carrito = Session::get('carrito');
            
            if(!$this->_checkItem($json->id)){//No existe elemento en el carrito
                $item = array();
                foreach ($json as $key => $value) {
                    $item[$key] = $value;
                }
                $carrito[$json->id] = $item;
                Session::set('carrito', $carrito);
                
            }
            else{//Existe key y procedemos a modificar la cantidad
                $carrito[$json->id]['cantidad'] += $json->cantidad;
                Session::set('carrito', $carrito);
            }
        }
        else{
            echo json_encode('Mal asunto');
        }
        echo json_encode(Session::get('carrito'));
    }
    
    
    public function deleteItem(){
        $carrito = array();
        header("Content-Type: text/json");
        
        if($_POST['json']){
            $json = json_decode($_POST['json']);
            $carrito = Session::get('carrito');
            
            if($this->_checkItem($json->id)){
                unset($carrito[$json->id]);
                Session::set('carrito', $carrito);
            }
            else{
                echo json_encode('No clave');
            }
        }else{
            echo json_encode('Mal asunto');
        }
        echo json_encode(Session::get('carrito'));
    }
    
    
    //Verificar si tenemos ese elemento en el carrito
    private function _checkItem($key){
        $carrito = Session::get('carrito');
        
        foreach($carrito as $item){
            if ($item['id'] == $key){
                return true;
            }
        }
        //Si no encuentra ninguna clave retornamos falso
        return false;
    }
}
