<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of noticiasController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class noticiasController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    //Ver varias noticias implementar con paginador
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $noticias = $this->loadModel('noticias');
        $this->_view->noticias = $paginador->paginar($noticias->getNoticias(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'noticias/index');
        $this->_view->titulo = 'Noticias';
        $this->_view->hook = 'Noticias';
        $this->_view->render('index', 'noticias');
    }
    
    //Ver una noticia
    public function verNoticia($id){
        $noticia = $this->loadModel('noticias');
        $this->_view->noticia = $noticia->getNoticia($this->filtrarInt($id));
        $this->_view->noticia = array_pop($this->_view->noticia);
        $this->_view->titulo = $this->_view->noticia['titulo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'noticias">Notícias</a> >> ' . $this->_view->noticia['titulo'];
        $this->_view->render('vernoticia', 'noticias');
    }
    
}
