<?php

class chicaController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $chica = $this->loadModel("chica");
        #$this->_view->chica = $chica->getChicaModa();
        $this->_view->chica = $paginador->paginar($chica->getChicaModa(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'chica/index');
        $this->_view->titulo = "Moda Mujeres";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Moda Mujeres';
        $this->_view->render('index', 'chica/');
    }
    
    public function verProducto($id){
        $chica = $this->loadModel("chica");
        $this->_view->chica = $chica->getChica($this->filtrarInt($id));
        $this->_view->chica = array_pop($this->_view->chica);
        $this->_view->titulo = $this->_view->chica['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'chica">Chicas</a> >> ' . $this->_view->chica['modelo'];
        $this->_view->render('verProducto', 'chicas');
    }
}