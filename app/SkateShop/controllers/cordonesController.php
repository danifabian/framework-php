<?php

class cordonesController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $cordo = $this->loadModel("cordones");
        #$this->_view->cordo = $cordo->getCordones();
        $this->_view->cordo = $paginador->paginar($cordo->getCordones(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'cordones/index');
        $this->_view->titulo = "Accesorios Cordones";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios Cordones';
        $this->_view->render('index', 'accesorios/cordones/');
    }
    
    public function verProducto($id){
        $cordo = $this->loadModel("cordones");
        $this->_view->cordo = $cordo->getCordon($this->filtrarInt($id));
        $this->_view->cordo = array_pop($this->_view->cordo);
        $this->_view->titulo = $this->_view->cordo['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'cordones">Cordones</a> >> ' . $this->_view->cordo['modelo'];
        $this->_view->render('verProducto', 'cordones');
    }
}