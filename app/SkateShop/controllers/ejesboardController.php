<?php


class ejesboardController extends Controller {
    
    public function __construct() {
        parent::__construct();
    }
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $ejes = $this->loadModel("ejesboard");
        #$this->_view->ejes = $ejes->getEjesBoard();
        $this->_view->ejes = $paginador->paginar($ejes->getEjesBoard(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'ejesboard/index');
        $this->_view->titulo = "Ejes";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >>Ejes';
        $this->_view->render('index', 'skateboard/ejes');
    }
    
    public function verProducto($id){
        $ejes = $this->loadModel("ejesboard");
        $this->_view->ejes = $ejes->getEjeBoard($this->filtrarInt($id));
        $this->_view->ejes = array_pop($this->_view->ejes);
        $this->_view->titulo = $this->_view->ejes['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'ejesboard">Ejes</a> >> ' . $this->_view->ejes['modelo'];
        $this->_view->render('verProducto', 'ejes');
    }
}