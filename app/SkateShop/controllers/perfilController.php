<?php

class perfilController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        Session::acceso('usuario');
        $users = $this->loadModel('usuarios');
        
        $this->_view->titulo = 'Perfil usuario';
        $this->_view->hook = 'Perfil de ' . Session::get('username');
        $this->_view->user = $users->getUser(Session::get('id_cliente'), Session::get('username'), Session::get('email'));
        $this->_view->render('index');
    }
    
    public function actualizarPerfil($id){
        Session::acceso('usuario');
        $perfil = $this->loadModel('usuarios');
        $this->_view->perfil = $perfil->getPerfil($this->filtrarInt($id));
        $this->_view->titulo = 'Actualizar perfil';
        $this->_view->hook = '<a href="' . BASE_URL . 'perfil">Perfil</a> >> Actualizar Perfil';
        $this->_view->render('modificarPerfil');
    }
    
    public function update(){
        Session::acceso('usuario');
        $perfil = $this->loadModel('usuarios');
        $this->_view->titulo = 'Perfil usuario';
        $this->_view->hook = 'Perfil de ' . Session::get('username');
   
        $perfil->updatePerfil(
            $this->getPostInt('id'),
            $this->getPostText('username'),
            $this->getPostText('email'),
            $this->getPostText('direccion'),
            $this->getPostText('provincia'),
            $this->getPostText('pueblo'),
            $this->getPostText('cod_postal')       
        );
         
        $this->_view->user = $perfil->getUser(Session::get('id_cliente'), Session::get('username'), Session::get('email'));
        $this->_view->render('index');
    }
    
    
     public function verFacturas($id){
        $clie = $this->loadModel("facturas");
        $this->_view->clie = $clie->getFacturas($id);
        $this->_view->titulo = 'Facturas';
        $this->_view->hook = '<a href="'.BASE_URL.'perfil">Inicio</a> >> <a href="' . BASE_URL . 'perfil">Ver facturas</a> >> Mis Facturas';
        $this->_view->render('verfacturas', 'perfil');
    }
}