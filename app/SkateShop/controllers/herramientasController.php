<?php

class herramientasController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $herra = $this->loadModel("herramientas");
        #$this->_view->herra = $herra->getHerramientas();
        $this->_view->herra = $paginador->paginar($herra->getHerramientas(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'herramientas/index');
        $this->_view->titulo = "Accesorios Herramientas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >>Accesorios Herramientas';
        $this->_view->render('index', 'accesorios/herramientas/');
    }
    
    public function verProducto($id){
        $goma = $this->loadModel("herramientas");
        $this->_view->herra = $goma->getHerramienta($this->filtrarInt($id));
        $this->_view->herra = array_pop($this->_view->herra);
        $this->_view->titulo = $this->_view->herra['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'herramientas">Herramientas</a> >> ' . $this->_view->herra['modelo'];
        $this->_view->render('verProducto', 'herramienta');
    }
}