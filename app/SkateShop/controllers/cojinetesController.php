<?php

class cojinetesController extends Controller {
    
    public function __construct() {
        parent::__construct();
    }
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $cojin = $this->loadModel("cojinetes");
        #$this->_view->cojin = $cojin->getCojinetes();
        $this->_view->cojin = $paginador->paginar($cojin->getCojinetes(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'cojinetes/index');
        $this->_view->titulo = "Cojinetes";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >>Cojinetes';
        $this->_view->render('index', 'cojinetes');
    }
    
    public function verProducto($id){
        $cojin = $this->loadModel("cojinetes");
        $this->_view->cojin = $cojin->getCojinete($this->filtrarInt($id));
        $this->_view->cojin = array_pop($this->_view->cojin);
        $this->_view->titulo = $this->_view->cojin['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'cojinetes">Cojinetes</a> >> ' . $this->_view->cojin['modelo'];
        $this->_view->render('verProducto', 'cojinetes');
    }
}