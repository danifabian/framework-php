<?php
class pueblosController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        echo "Puebloc Controller - index method";
    }
    
    public function getPueblo(){
        header("Content-Type: text/json");
        if(isset($_POST['idSelected'])){
            $id = $this->filtrarInt($_POST['idSelected']);
            $mod = $this->loadModel('pueblos');
            $pueblos = $mod->getPueblos($id);
            print_r(json_encode($pueblos));
        }
        else{
            echo json_encode('No se a enviado nada');
        }
    }
    
    public function getCp(){
        header("Content-Type: text/json");
        if(isset($_POST['idSelected'])){
            $id = $this->filtrarInt($_POST['idSelected']);
            $mod = $this->loadModel('pueblos');
            $cp = $mod->getCp($id);
            print_r(json_encode($cp));
        }
        else{
            echo json_encode('No se a enviado nada');
        }
    }
}