<?php

class lijaController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $lija = $this->loadModel("lija");
        #$this->_view->lija = $lija->getLijas();
        $this->_view->lija = $paginador->paginar($lija->getLijas(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'lija/index');
        $this->_view->titulo = "Accesorios Lijas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios Lijas';
        $this->_view->render('index', 'accesorios/lija/');
    }
    
    public function verProducto($id){
        $lija = $this->loadModel("lija");
        $this->_view->lija = $lija->getLija($this->filtrarInt($id));
        $this->_view->lija = array_pop($this->_view->lija);
        $this->_view->titulo = $this->_view->lija['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'accesorios">Lijas</a> >> ' . $this->_view->lija['modelo'];
        $this->_view->render('verProducto', 'lija');
    }
}