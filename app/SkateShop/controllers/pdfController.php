<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pdfController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class pdfController extends Controller{
    private $_pdf;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){}
    
    public function pdffactura($codFac){
        $x =10;
        $y=170;
        $total=0;
        $this->getLibrary('fpdf');
        $this->_pdf = new FPDF();
        $pdf = $this->loadModel("facturas");
        $this->_view->con = $pdf->getFactura($codFac);
        $this->_pdf->AddPage();
        $this->_pdf->SetFont('Arial','B',20);
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, 'Nombre de la tienda');
        $this->_pdf->Ln(20);
        $this->_pdf->SetFont('Arial','',14);
        $this->_pdf->Cell(40,10, 'PEDIDO REALIZADO');
        $this->_pdf->Ln(5);
        $this->_pdf->Cell(40,10, '--------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Ln(10);
        $this->_pdf->SetFont('Arial','',12);
        $this->_pdf->Cell(40,10, utf8_decode('Muchas gracias por tu pedido. Si necesitas ayuda puedes llamar a nuestro teléfono gratuito '));
        $this->_pdf->Ln(5);
        $this->_pdf->Cell(40,10, utf8_decode('de atención al cliente XXX-XXX-XXX'));
        $this->_pdf->Ln(20);
        $this->_pdf->Cell(40,10,'Fecha de pedido : '.$this->_view->con[0]['fecha_pedido']);
        $this->_pdf->Ln(5);
        $this->_pdf->Cell(40,10,'Numero de pedido: '.$this->_view->con[0]['codigo_pedido']);
        $this->_pdf->Ln(10);
        $this->_pdf->Cell(40,10, '---------------------------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Ln(10);
        $this->_pdf->Cell(40,10, utf8_decode('DIRECCIÓN DE ENVÍO'));
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, utf8_decode('DIRECCIÓN DE FACTURACIÓN'));
        $this->_pdf->Ln(10);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['nombre'].' '.$this->_view->con[0]['apellidos']);
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['nombre'].' '.$this->_view->con[0]['apellidos']);
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['direccion']);
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, utf8_decode("España"));
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['cod_postal'].' '.$this->_view->con[0]['pueblo']);
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['numero']);
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['provincia']);
        $this->_pdf->Cell(60);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['email']);
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, utf8_decode("España"));
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, $this->_view->con[0]['numero']);
        $this->_pdf->Ln(10);
        $this->_pdf->Cell(40,10, '---------------------------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, utf8_decode("PRODUCTO"));
        $this->_pdf->Cell(1);
        $this->_pdf->Cell(65,10, utf8_decode("DESCRIPCIÓN"));
        $this->_pdf->Cell(1);
        $this->_pdf->Cell(30,10, utf8_decode("TALLA"));
        $this->_pdf->Cell(1);
        $this->_pdf->Cell(30,10, utf8_decode("CANTIDAD"));
        $this->_pdf->Cell(1);
        $this->_pdf->Cell(20,10, utf8_decode("IMPORTE"));
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, '---------------------------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Ln(20);
        foreach($this->_view->con as $con):
            $this->_pdf->Image(''. $con['img_url'].'',$x,$y,10);
            $this->_pdf->Cell(30);
            $this->_pdf->Cell(20,10, $con['modelo']);
            $this->_pdf->Cell(55);
            $this->_pdf->Cell(20,10, $con['dimensiones']);
            $this->_pdf->Cell(20);
            $this->_pdf->Cell(20,10, $con['cant']);
            $this->_pdf->Cell(10);
            $this->_pdf->Cell(20,10, $con['precio']*$con['cant'] . iconv('UTF-8', 'windows-1252', ' €'));
            $precio = $con['precio']*$con['cant'];
            $total = $total + $precio;
            $this->_pdf->Ln(40);
            $y = $y+40;
            if ($y > 280):
                $this->_pdf->AddPage();
                $y=20;
                $this->_pdf->Ln(10);
            endif;
        endforeach;
        $this->_pdf->Cell(40,10, '---------------------------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(150);
        $this->_pdf->Cell(10,10, utf8_decode("TOTAL"));
        $this->_pdf->Cell(10);
        $this->_pdf->Cell(20,10, $total .  iconv('UTF-8', 'windows-1252', ' €'));
        $this->_pdf->Ln(6);
        $this->_pdf->SetFont('Arial','B',10);
        $this->_pdf->Cell(170);
        $this->_pdf->Cell(10,10, utf8_decode("IVA Incluido"));
        $this->_pdf->SetFont('Arial','',12);
        $this->_pdf->Ln(6);
        $this->_pdf->Cell(40,10, '---------------------------------------------------------------------------------------------------------------------------------------');
        $this->_pdf->Output();
    }
}
