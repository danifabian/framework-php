<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class usuariosController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->_view->titulo = 'Login / Registro';
        $this->_view->hook = 'Login / Registro de usuario';
        $this->_view->render('index');
    }
    
    
    public function login(){
        $users = $this->loadModel('login');
        if(isset($_POST['login-btn']))
            $user = $users->login($this->mysql_escape_mimic('login-email'), $this->mysql_escape_mimic('login-password'));
        
        if($user != null){
            Session::set('userAuth', true);
            Session::set('id_cliente', $user['id_cliente']);
            Session::set('username', $user['username']);
            Session::set('email', $user['email']);
            Session::set('level', $user['role']);
            Session::set('tiempo', time());
            $this->redireccionar();
        }
        else{//Mostrar mensaje de error
            $this->_view->titulo = 'Login / Registro';
            $this->_view->hook = 'Login / Registro de usuario';
            $this->_view->_error = 'Email o password incorrectos';
            $this->_view->render('index');
        }
    }
    
    public function registro(){
        $registro = $this->loadModel('registro');
        //Parametros de entrda
        $this->_view->titulo = 'Login / Registro';
        $this->_view->hook = 'Login / Registro de usuario';
        
        if($this->getPostParam('reg-btn')){//Existe click en boton registro
            //Guardar array de datos con los datos enviados por POST
            $this->_view->data  = $_POST;
            
            //Validacion y comprobacion de datos
            if($registro->getUsername($this->getAlphaNum('registro-username'))){
                $this->_view->_error = 'El usuario ' . $this->getAlphaNum('registro-username') .' ya está registrado';
                $this->_view->render('index');
                exit;
            }
            
            //Verificar email y comprobar si existe en la BD
            if(!$this->validarEmail($this->getPostParam('registro-email'))){
                $this->_view->_error = 'El email introducido es incorrecto';
                $this->_view->render('index');
                exit;
            }
            if($registro->getEmail($this->getPostParam('registro-email'))){
                $this->_view->_error = 'El email ' . $this->getPostParam('registro-email') . ' ya está registrado';
                $this->_view->render('index');
                exit;
            }
            
            //Verificar password
            $this->getAlphaNum('registro-pass');
            if(!$this->mysql_escape_mimic('registro-pass')){
                $this->_view->_error = 'Has introducido carácteres no permitidos';
                $this->_view->render('index');
                exit;
            }
            $this->getAlphaNum('confirmar');
            if($this->mysql_escape_mimic('registro-pass') != $this->mysql_escape_mimic('confirmar')){
                $this->_view->_error = 'Las contraseñas no coinciden';
                $this->_view->render('index');
                exit;
            }
            
            if(!$this->getPostText('registro-name')){
                $this->_view->_error = 'Es necesario ingresar el nombre de usuario';
                $this->_view->render('index');
                exit;
            }
            
            
            //Si ejectua este codigo a pasado todas las validaciones y podemos hacer
            //el ingreso de los datos en la bd
            $id = $registro->registrarCliente(//Guardar cliente
                $this->getPostText('registro-name'), 
                $this->getPostText('registro-apellidos'), 
                $this->getPostText('registro-direccion'), 
                $this->getPostInt('registro-pueblo'), 
                $this->getPostParam('registro-nacdate'),
                $this->getPostParam('registro-email')
            );
            
            $registro->registrarTelefono(//Guardar telefonos de un cliente
                $id['LAST_INSERT_ID()'],
                $this->getPostInt('registro-movil')                 
            );
            if($this->getPostInt('registro-fijo')!=NULL){
                $registro->registrarTelefono(//Guardar telefonos de un cliente
                    $id['LAST_INSERT_ID()'],
                    $this->getPostInt('registro-fijo')    
                );
            }
            $registro->registrarUsuario(//Guardar usuario
                $this->getAlphaNum('registro-username'),
                $id['LAST_INSERT_ID()'],
                $this->mysql_escape_mimic('registro-pass'),
                'usuario'
            );
            
            //Ultimas comprobaciones del registro
            if(!$registro->getUsername($this->getAlphaNum('registro-username'))){
                $this->_view->_error = 'Problemas al registrar usuario';
                $this->_view->render('index');
                exit;
            }
            if(!$registro->getIdClie($registro->getIdClieUser($this->getAlphaNum('registro-username')))){
                $this->_view->_error = 'Problemas al registrar cliente';
                $this->_view->render('index');
                exit;
            }
        }
        //no es error
        $this->_view->_error = 'Usuario registrado correctamente';
        $this->_view->render('index');
    }
    
    public function cerrar_sesion(){
        Session::destroy();
        $this->redireccionar('');
    }
}
