<?php

class longboardController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $long = $this->loadModel("longboard");
        #$this->_view->long = $long->getLongboard();
        $this->_view->long = $paginador->paginar($long->getLongboard(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'longboard/index');
        $this->_view->titulo = "Longboard";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >>Longboard';
        $this->_view->render('index', 'longboard');
    }
    
    public function verProducto($id){
        $long = $this->loadModel("longboard");
        $this->_view->long = $long->getLongProducto($this->filtrarInt($id));
        $this->_view->long = array_pop($this->_view->long);
        $this->_view->titulo = $this->_view->long['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'longboard">Producto</a> >> ' . $this->_view->long['modelo'];
        $this->_view->render('verProducto', 'productos');
    }
}
