<?php

class chicoController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $chico = $this->loadModel("chico");
        #$this->_view->chico = $chico->getChicoModa();
        $this->_view->chico = $paginador->paginar($chico->getChicoModa(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'chico/index');
        $this->_view->titulo = "Moda Hombres";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Moda Hombres';
        $this->_view->render('index', 'chico/');
    }
    
    public function verProducto($id){
        $chico = $this->loadModel("chico");
        $this->_view->chico = $chico->getChico($this->filtrarInt($id));
        $this->_view->chico = array_pop($this->_view->chico);
        $this->_view->titulo = $this->_view->chico['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >><a href="'. BASE_URL . 'chico">Chicos</a> >> ' . $this->_view->chico['modelo'];
        $this->_view->render('verProducto', 'chicos');
    }
}

