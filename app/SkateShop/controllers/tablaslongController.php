<?php

class tablaslongController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $tablas = $this->loadModel("tablaslong");
        #$this->_view->tablas = $tablas->getTablasLong();
        $this->_view->tablas = $paginador->paginar($tablas->getTablasLong(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'tablaslong/index');
        $this->_view->titulo = "Tablas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >>Tablas';
        $this->_view->render('index', 'longboard/tablas');
    }
    
    public function verProducto($id){
        $tablas = $this->loadModel("tablaslong");
        $this->_view->tablas = $tablas->getTablaLong($this->filtrarInt($id));
        $this->_view->tablas = array_pop($this->_view->tablas);
        $this->_view->titulo = $this->_view->tablas['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'tablaslong">Tablas</a> >> ' . $this->_view->tablas['modelo'];
        $this->_view->render('verProducto', 'tablas');
    }
}