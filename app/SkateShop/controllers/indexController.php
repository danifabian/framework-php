<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of indexController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class indexController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }


    public function index(){
        $noticias = $this->loadModel('noticias');
        $articulos = $this->loadModel('productos');
        $this->_view->noticias = $noticias->getNoticiasInicio();
        $this->_view->articulos = $articulos->getProductosInicio();
        
        $this->_view->titulo = 'Inicio';
        $this->_view->hook = 'Inicio';
        $this->_view->render('index', 'inicio');
    }
    
}
