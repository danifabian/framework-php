<?php
/**
 * Description of comprarController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class compraritemsController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){//Redireccionar a un metodo o otro segun la sesion de usuario
        if(Session::get('userAuth')){
            $this->redireccionar('compraritems/realizarPagoUsuario');
        }
        else{
            $this->redireccionar('compraritems/nuevoCliente');
        }
    }
    
    public function nuevoCliente(){
        $registro = $this->loadModel('registro');
        $this->_view->titulo = 'Datos de cliente';
        $this->_view->hook = 'Registrar datos para envio';
        
        if($this->getPostParam('reg-btn')){//Existe click en boton registro
            //Guardar array de datos con los datos enviados por POST
            $this->_view->data  = $_POST;
            
            //Verificar email y comprobar si existe en la BD
            if(!$this->validarEmail($this->getPostParam('registro-email'))){
                $this->_view->_error = 'El email introducido es incorrecto';
                $this->_view->render('newClie');
                exit;
            }
            if(!$this->getPostText('registro-name')){
                $this->_view->_error = 'Es necesario ingresar el nombre de usuario';
                $this->_view->render('newClie');
                exit;
            }
            if(!$this->getPostText('registro-apellidos')){
                $this->_view->_error = 'Es necesario ingresar los apellidos';
                $this->_view->render('newClie');
                exit;
            }
            if(!$this->getPostText('registro-direccion')){
                $this->_view->_error = 'Si no introduce su dirección no podremos realizar el envio';
                $this->_view->render('newClie');
                exit;
            }
            //Si ejectua este codigo a pasado todas las validaciones y podemos hacer
            //el ingreso de los datos en la bd
            $id = $registro->registrarCliente(//Guardar cliente
                $this->getPostText('registro-name'), 
                $this->getPostText('registro-apellidos'), 
                $this->getPostText('registro-direccion'), 
                $this->getPostInt('registro-pueblo'), 
                $this->getPostParam('registro-nacdate'),
                $this->getPostParam('registro-email')                 
            );
            // Conseguimos el id del cliente e insertamos los telefonos de dicho cliente
            //echo ' id ' . $id['LAST_INSERT_ID()']. ' - ' .$this->getPostInt('registro-movil') ;
            $registro->registrarTelefono(//Guardar telefonos de un cliente
                $id['LAST_INSERT_ID()'],
                $this->getPostInt('registro-movil')
                 
            );
            if($this->getPostInt('registro-fijo')!=NULL){
                $registro->registrarTelefono(//Guardar telefonos de un cliente
                    $id['LAST_INSERT_ID()'],
                    $this->getPostInt('registro-fijo')    
                );
            }
            if(!$registro->getEmail($this->getPostParam('registro-email'))){
                $this->_view->_error = 'Problemas al registrar cliente';
                $this->_view->render('newClie');
                exit;
            }
            //Si pasa todas las validaciones redireccionaremos a pagina con informacion
            // de la compra y la posibilidad de descargar con fpdf en formato pdf
            $this->redireccionar('compraritems/crearFactura/' . $id['LAST_INSERT_ID()']);
        }
        else{
            $this->_view->render('newClie');
        }
    }
    
    //En caso de haber sesion de usuario de redirecciona a este metodo
    //para recoger los datos de sesion de usuario y realizar el pago
    public function realizarPagoUsuario(){
        $model = $this->loadModel('usuarios'); 
        $this->_view->titulo = 'Datos de cliente';
        $this->_view->hook = 'Registrar datos para envio';
        Session::acceso('usuario');
        $this->_view->user = $model->getUser(Session::get('id_cliente'), Session::get('username'),Session::get('email'));
        $this->_view->render('pagoUser');
    }
    
    
    //Despues de introducir datos de tarjeta se redirecciona aqui para
    //para guardar factura y lineas factura de todas las compras
    public function crearFactura($idClie){
        $facturas = $this->loadModel('facturas');
        $lineasFact = $this->loadModel('lineasfact');
        $producto = $this->loadModel('productos');
        do{
            $cod = $this->generarCodigoPedido();
            
        }
        while($facturas->issetCodProd($cod)!=false);
        
        $fact = $facturas->setFactura($idClie, $cod, 21);
        
        $i=0;
        foreach (Session::get('carrito') as $key){
            $lineasFact->setLineasFact($fact['id_factura'], $key, 21, $i);
            $pro=$producto->getProductoId($key);
            $lineasFact->updateCantidad($pro,$key);
            $i++;
        }
        $this->redireccionar('compraritems/mostrarDatosCompra/'.$cod);        
    }
    
    //Muestra datos de la compra y da opcion a ver en pdf para descargar
    public function mostrarDatosCompra($codFac){
        $data = array();
        foreach(Session::get('carrito') as $item){
            $data[$item['id']] = $item['cantidad']*$item['precio'];
        }
        $this->_view->titulo = 'Gracias por su compra';
        $this->_view->hook = 'Gracias por su compra';
        $this->_view->data = Session::get('carrito');
        $this->_view->totalPrice = $data;
        $this->_view->cod = $codFac;
        Session::destroy('carrito');
        $this->_view->render('mostrarCompra');
    }
    
    
}
