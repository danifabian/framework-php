<?php
/**
 * Description of adminpanelController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class adminpanelController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        Session::acceso('gestor');
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $noticias = $this->loadModel('noticias');
        $this->_view->noticias = $paginador->paginar($noticias->getNoticias(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'adminpanel/index');
        $this->_view->titulo = 'Admin panel';
        $this->_view->hook = 'Panel administrador';
        $this->_view->render('index');
    }
    
    public function verNoticia($id){
        Session::acceso('gestor');
        $noticia = $this->loadModel('noticias');
        $this->_view->noticia = $noticia->getNoticia($this->filtrarInt($id));
        $this->_view->noticia = array_pop($this->_view->noticia);
        $this->_view->titulo = $this->_view->noticia['titulo'];
        $this->_view->hook = '<a href="'. BASE_URL . 'adminpanel">Panel administrador</a> >> Notícias >> ' . $this->_view->noticia['titulo'];
        $this->_view->render('verNoticia', 'noticias');
    }
    
    public function delete($id){
        Session::acceso('gestor');
        $noticias = $this->loadModel('noticias'); 
        $noticia = $noticias->getNoticia($this->filtrarInt($id));
        $data = $noticia[0];
        
        $noticias->deleteNoticia($this->filtrarInt($id));
        $noticia = $noticias->getNoticia($this->filtrarInt($id));
        
        $exp = explode('/', $data['img_url']);
        $exp = array_filter($exp);
        
        if($noticia == null){
            echo 'Elimina noticia';
            if(file_exists($data['img_url'])){//Existe la imagen y procedemos a "eliminarla"
                if(!in_array('default', $exp, true)){
                    $destino = 'public/upload/noticias/removed/' . $exp[2] . $exp[3];
                    copy($data['img_url'], $destino);
                    unlink($data['img_url']);
                }
                else{
                    echo ' existe default y no hacemos nada';
                }
            }
            else{
                echo 'No existe el elemento';
            }
            $this->redireccionar('adminpanel');
        }
        else{
             echo 'Error al eliminar  noticia';
        }
    }
    
    public function nuevaNoticia(){
        Session::acceso('gestor');
        $this->_view->titulo = 'Añdir notífia';
        $this->_view->hook = '<a href="' . BASE_URL .'adminpanel" >Panel administrador</a> >> Nueva notícia';
        $this->_view->render('nuevaNoticia');
/*id_noticia INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  titulo VARCHAR(200) NULL,
  cuerpo VARCHAR(255) NULL,
  fecha_registro DATETIME NULL,
  fecha_modificacion DATETIME NULL,
  img_url VARCHAR(255) NULL,*/
    }
    
    public function create(){
        Session::acceso('gestor');
        $this->_view->titulo = 'Añdir notícia';
        $this->_view->hook = '<a href="' . BASE_URL .'adminpanel" >Panel administrador</a> >> Nueva notícia';
        
        if($_POST['btn-submit']){//Hay envio de formulario
            $noticias = $this->loadModel('noticias');
            $upload_dir = 'public/upload/noticias/';
            $img_name = str_replace(' ', '_', $this->getPostText('titulo'));
            
            if(!$this->retornarFormato($_FILES['imagen-noticia']['type'])){
                $upload_dir = $upload_dir . 'default/2.gif';
            }
            else{//Existe la imagen
                if($_FILES['imagen-noticia']['error'] === 0){//No hay error de envio de imagen
                    //Validar formato de entrada de las iamgenes
                    if(!$this->validarFormatoValido($_FILES['imagen-noticia']['type'])){
                        $this->_view->_error = 'Formato de la imagen no está admitido';
                        $this->_view->render('nuevaNoticia');
                        exit;
                    }
                }
                //No tiene errores
                $upload_dir = $upload_dir . $img_name . '.' . $this->retornarFormato($_FILES['imagen-noticia']['type']);
                move_uploaded_file($_FILES['imagen-noticia']['tmp_name'], $upload_dir);
            }
            
            $noticias->createNoticia(
                Session::get('username'),
                $this->getPostText('titulo'),
                $this->getPostText('cuerpo'),
                $upload_dir
            );              
            
        }
        $this->redireccionar('adminpanel');
    }
    
    public function actualizarNoticia($id){
        Session::acceso('gestor');
        $noticias = $this->loadModel('noticias');
        $this->_view->noticia = $noticias->getNoticia($this->filtrarInt($id));
        
        $this->_view->titulo = 'Actualizar noticia';
        $this->_view->hook = '<a href="' . BASE_URL . 'adminpanel">Panel administrador</a> >> Actualizar noticia';
        $this->_view->render('modificarNoticia');
    }
    
    public function update(){
        Session::acceso('gestor');
        $noticias = $this->loadModel('noticias');
        $this->_view->titulo = 'Ver noticia';
        $this->_view->hook = '<a href="' . BASE_URL . 'adminpanel">Panel administrador</a> >> Actualizar noticia';
        
        if($this->getPostParam('btn-submit')){
            if($_FILES['imagen-noticia']['error'] == UPLOAD_ERR_OK){
                $upload_dir = 'public/upload/noticias/';
                $img_name = str_replace(' ', '_', $this->getPostText('titulo'));
                $noticiaActual = $noticias->getNoticia($this->getPostInt('data'));
                $img_url = explode('/', $noticiaActual[0]['img_url']);
                
                if(!in_array('default', $img_url)){
                    //echo 'img url actual: es una imagen subida previamente al servidor';
                    $removed_dir = $upload_dir . 'removed/' . $this->getPostInt('data') . '_' . $img_name . '.' . $this->retornarFormato($_FILES['imagen-noticia']['type']);
                    copy($noticiaActual[0]['img_url'], $removed_dir);
                    unlink($noticiaActual[0]['img_url']);
                    $upload_dir = $upload_dir . $img_name . '.' . $this->retornarFormato($_FILES['imagen-noticia']['type']);
                    move_uploaded_file($_FILES['imagen-noticia']['tmp_name'], $upload_dir);
                }
                else{
                    //Contiene default y vamos a modificar por una neuva
                    $upload_dir = $upload_dir . $img_name . '.' . $this->retornarFormato($_FILES['imagen-noticia']['type']);
                    move_uploaded_file($_FILES['imagen-noticia']['tmp_name'], $upload_dir);
                    
                }
                
                $noticias->updateNoticia(
                    $this->getPostInt('data'),
                    Session::get('username'),
                    $this->getPostText('titulo'),
                    $this->getPostText('cuerpo'),
                    $upload_dir
                );
            }else{
                //echo 'Vamos a modificar solo contenidos';
                $noticias->updateNoticia(
                    $this->getPostInt('data'),
                    Session::get('username'),
                    $this->getPostText('titulo'),
                    $this->getPostText('cuerpo'),
                    null
                );
            }
        }
        $this->_view->noticia = $noticias->getNoticia($this->getPostInt('data'));
        $this->_view->noticia = array_pop($this->_view->noticia);
        $this->redireccionar('adminpanel/verNoticia/'.$this->getPostInt('data'));
    }
}
