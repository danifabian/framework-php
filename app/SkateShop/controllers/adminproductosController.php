<?php

class adminproductosController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        Session::acceso('gestor');
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $productos = $this->loadModel('productos');
        
        $this->_view->listado = $paginador->paginar($productos->listadoProductos(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'adminproductos/index');
        $this->_view->titulo = 'Listado productos';
        $this->_view->hook = '<a href="' . BASE_URL .'adminpanel">Inicio</a> >> Listado de productos';
        $this->_view->render('index', '');
    }
    
    /* Vista */
    public function nuevoProducto(){
        Session::acceso('gestor');
        $marcas = $this->loadModel('marcas');
        $categorias = $this->loadModel('categoria');
        
        $this->_view->titulo = 'Nuevo producto';
        $this->_view->hook = '<a href="' . BASE_URL .'adminpanel">Inicio</a> >> <a href="' . BASE_URL .'adminproductos">Listado de productos</a> >> Crear producto';
        $this->_view->marcas = $marcas->getMarcas();
        $this->_view->categorias = $categorias->getCategorias();
        $this->_view->render('insertarProd');
    }
    public function createProd(){
        Session::acceso('gestor');
        if($this->getPostParam('btn-submit')){
            $prods = $this->loadModel('productos');
            $marcas = $this->loadModel('marcas');
            $upload_dir = 'public/upload/productos/';
            
            //Gestion de la iamgen
            if($_FILES['img-prod']['error'] === UPLOAD_ERR_NO_FILE){//No se a subido una imagen
                $upload_dir = $upload_dir . 'default/default2.gif';
                echo $upload_dir;
            }
            else if($_FILES['img-prod']['error'] === UPLOAD_ERR_OK){//Se a subido una imagen correctamente
                if($this->validarFormatoValido($_FILES['img-prod']['type'])){
                    $formato = $this->retornarFormato($_FILES['img-prod']['type']);
                    $marca = $marcas->getSoloMarca($this->getPostInt('marca'));
                    switch ($this->getPostInt('categorias')){
                        case 1://skateobard
                            $img_name = 'skateboard_' . $marca['nombre'] . '_' . $this->getAlphaNum('modelo') . '_' . $this->getPostText('dimension') . '.' . $formato;
                            $upload_dir = $upload_dir . "skates/" . $this->getPostText('tipo');
                            break;
                        case 2://longboard
                            $img_name = 'longboard_' . $marca['nombre'] . '_' . $this->getAlphaNum('modelo') . '_' . $this->getPostText('dimension') . '.' . $formato;
                            $upload_dir = $upload_dir . "skates/" . $this->getPostText('tipo');
                            break;
                        case 3://accesorios
                            $img_name = 'accesorios_' . $marca['nombre'] . '_' . $this->getAlphaNum('modelo') . '_' . $this->getPostParam('color') . '.' . $formato;
                            $upload_dir = $upload_dir . "accesorios/" . $this->getPostText('tipo');
                            break;
                        case 4://ropa chico
                            $img_name = 'chico_' . $this->getPostText('dimension') . '_' . $marca['nombre'] . '_' . $this->getAlphaNum('modelo') . '_' . $this->getPostParam('color') . '.' . $formato;
                            $upload_dir = $upload_dir . "ropa/" . $this->getPostText('tipo');
                            break;
                        case 5://ropa chica
                            $img_name = 'chica_' . $this->getPostText('dimension') . '_' . $marca['nombre'] . '_' . $this->getAlphaNum('modelo') . '_' . $this->getPostParam('color') . '.' . $formato;
                            $upload_dir = $upload_dir . "ropa/" . $this->getPostText('tipo');
                            break;
                    }
                }
                else{
                    $this->_view->_error = 'Formato de la imagen no es valido';
                    $this->_view->render('insertarProd');
                    exit;
                }
                //Comprobar directorios y subir la iamgen al servidor
                if(is_dir($upload_dir) && is_readable($upload_dir)){
                    $img_name = str_replace(' ', '_', $img_name);
                    $img_name = str_replace('"', '-', $img_name);//Filtrar comillas
                    $img_name = str_replace('&quot;', '-', $img_name);//Filtrar comillas en formato html &quot;
                    $upload_dir = $upload_dir . '/' . $img_name;
                    move_uploaded_file($_FILES['img-prod']['tmp_name'], $upload_dir);
                }
                else{
                    $this->redireccionar('err/acceso/4848');
                }
            }
            /* Filtrar elementos y comprobar antes de insertar en la tabla */
            if($this->getPostText('destacado') == 'on'){ $destacado = 1; }
            else { $destacado = 0; }
            if($this->getPostInt('peso')){ $peso = $this->getPostInt('peso'); }
            else{ $peso = 'null'; }
            //Realizar insercion en la bd
            $prod = $prods->createProd(
                    $this->getPostInt('marca'),
                    $this->getPostInt('categorias'),
                    date(DateTime::ATOM),
                    $this->getPostText('descripcion'),
                    $this->getPostInt('stock'),
                    $this->getPostInt('precio'),
                    $this->getAlphaNum('modelo'),
                    $this->getPostText('dimension'),
                    $peso,
                    $this->getPostText('concavo'),
                    $this->getPostText('color'),
                    $upload_dir,
                    $destacado,
                    $this->getPostText('tipo')
                );
            $this->redireccionar('adminproductos');
        }
        else{
            $this->redireccionar('err/acceso/401');
        }
    }
    
    public function actualizarProd($id){
        Session::acceso('gestor');
        $prods = $this->loadModel('productos');
        $marcas = $this->loadModel('marcas');
        $this->_view->titulo = 'Actualizar producto';
        $this->_view->hook = '<a href="' . BASE_URL .'adminpanel">Inicio</a> >> <a href="' . BASE_URL .'adminproductos">Listado de productos</a> >> Actualizar producto';
        $this->_view->producto = $prods->getProducto($this->filtrarInt($id));
        $this->_view->marcas = $marcas->getMarcas();
        $this->_view->render('modificarProd');
    }
    public function updateProd(){
        Session::acceso('gestor');
        if($this->getPostParam('btn-submit')){
            $upload_dir = 'public/upload/productos/';
            $productos = $this->loadModel('productos');
            $marcas = $this->loadModel('marcas');
            $prod = $productos->getProducto($this->getPostInt('data'));
            if($_FILES['img-prod']['error'] == UPLOAD_ERR_NO_FILE){
                //El usuario no ha subido imagen y se utilizara la que hay en la bd
                $upload_dir = $prod['img_url'];
            }
            else if($_FILES['img-prod']['error'] == UPLOAD_ERR_OK){
                //Se ha subido la nueva imagen al servidor
                $formato = $this->retornarFormato($_FILES['img-prod']['type']);
                $marca = $marcas->getSoloMarca($this->getPostInt('marca'));
                switch ($this->getPostInt('dataCat')){
                    case 1://skateobard
                        $img_name = 'skateboard_' . $marca['nombre'] . '_' . $this->getAlphaNum('dataMod') . '_' . $this->getPostText('dataDim') . '.' . $formato;
                        $upload_dir = $upload_dir . "skates/" . $this->getPostText('dataType');
                        break;
                    case 2://longboard
                        $img_name = 'longboard_' . $marca['nombre'] . '_' . $this->getAlphaNum('dataMod') . '_' . $this->getPostText('dataDim') . '.' . $formato;
                        $upload_dir = $upload_dir . "skates/" . $this->getPostText('dataType');
                        break;
                    case 3://accesorios
                        $img_name = 'accesorios_' . $marca['nombre'] . '_' . $this->getAlphaNum('dataMod') . '_' . $this->getPostParam('color') . '.' . $formato;
                        $upload_dir = $upload_dir . "accesorios/" . $this->getPostText('dataType');
                        break;
                    case 4://ropa chico
                        $img_name = 'chico_' . $this->getPostText('dataDim') . '_' . $marca['nombre'] . '_' . $this->getAlphaNum('dataMod') . '_' . $this->getPostParam('color') . '.' . $formato;
                        $upload_dir = $upload_dir . "ropa/" . $this->getPostText('dataType');
                        break;
                    case 5://ropa chica
                        $img_name = 'chica_' . $this->getPostText('dataDim') . '_' . $marca['nombre'] . '_' . $this->getAlphaNum('dataMod') . '_' . $this->getPostParam('color') . '.' . $formato;
                        $upload_dir = $upload_dir . "ropa/" . $this->getPostText('dataType');
                        break;
                }
                //Comprobar directorios y subir la iamgen al servidor
                if(is_dir($upload_dir) && is_readable($upload_dir)){
                    $img_name = str_replace(' ', '_', $img_name);
                    $img_name = str_replace('"', '-', $img_name);//Filtrar comillas
                    $img_name = str_replace('&quot;', '-', $img_name);//Filtrar comillas en formato html &quot;
                    $upload_dir = $upload_dir . '/' . $img_name;
                    move_uploaded_file($_FILES['img-prod']['tmp_name'], $upload_dir);
                }
                else{
                    $this->redireccionar('err/acceso/4848');
                }
            }
            if($this->getPostText('destacado') == 'on'){ $destacado = 1; }
            else { $destacado = 0; }
            $productos->updateProducto(
                $this->getPostInt('data'), 
                $this->getPostInt('marca'),
                $this->getPostInt('stock'),
                $this->getPostText('precio'),
                $this->getPostText('color'),
                $destacado,
                $upload_dir
            );
            $this->redireccionar('adminproductos');
        }
        else{
            $this->redireccionar('err/acceso/401');
        }
        
        
        echo '<pre>';
        print_r($_POST);
        print_r($_FILES);
        
    }
    /* Solo el jefazo(admin) puede eliminar articulos */
    public function eliminarProd($id){
        Session::acceso('gestor');
        $prod = $this->loadModel('productos');
        $prod->deleteItem($this->filtrarInt($id));
        $this->redireccionar('adminproductos');
    }
}