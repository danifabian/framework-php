<?php

class buscadorController extends Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index($pagina = false) {
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $buscar = $this->loadModel("productos");
        
        $item=$this->getPostText('buscador');
        
        $this->_view->buscar = $paginador->paginar($buscar->buscadorProducto($item), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'buscador/index');
        $this->_view->titulo = "A buscado: " . $this->getPostText('buscador');
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Productos';
        $this->_view->render('index', 'buscador');
    }
    
    public function verProducto($id){
        $buscar = $this->loadModel("skateboard");
        $this->_view->buscar = $buscar->getProducto($this->filtrarInt($id));
        $this->_view->buscar = array_pop($this->_view->buscar);
        $this->_view->titulo = $this->_view->buscar['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> ' . $this->_view->buscar['modelo'];
        $this->_view->render('verProducto', 'buscador');
    }
}

