<?php

class tablasboardController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $tablas = $this->loadModel("tablasboard");
        #$this->_view->tablas = $tablas->getTablasBoard();
        $this->_view->tablas = $paginador->paginar($tablas->getTablasBoard(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'tablasboard/index');
        $this->_view->titulo = "Tablas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Tablas';
        $this->_view->render('index', 'skateboard/tablas');
    }
    
    public function verProducto($id){
        $skate = $this->loadModel("tablasboard");
        $this->_view->skate = $skate->getTablaBoard($this->filtrarInt($id));
        $this->_view->skate = array_pop($this->_view->skate);
        $this->_view->titulo = $this->_view->skate['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'tablasboard">Tablas</a> >> ' . $this->_view->skate['modelo'];
        $this->_view->render('verProducto', 'tablas');
    }
}
