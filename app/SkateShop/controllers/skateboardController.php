<?php

class skateboardController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $skate = $this->loadModel("skateboard");
        $this->_view->tablas = $paginador->paginar($skate->getSkateboard(), $pagina);
        #$this->_view->tablas = $skate->getSkateboard();
        $this->_view->paginacion = $paginador->getView('paginador', 'skateboard/index');
        $this->_view->titulo = "Skateboard";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Skateboard';
        $this->_view->render('index', 'skateboard');
    }
    
    public function verProducto($id){
        $skate = $this->loadModel("skateboard");
        $this->_view->skate = $skate->getProducto($this->filtrarInt($id));
        $this->_view->skate = array_pop($this->_view->skate);
        $this->_view->titulo = $this->_view->skate['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >><a href="'. BASE_URL . ''.$this->_view->skate['categoria'].'">Producto</a> >> ' . $this->_view->skate['modelo'];
        $this->_view->render('verproducto', 'productos');
    }
}
