<?php

class completolongController extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $completolong = $this->loadModel("completolong");
        #$this->_view->long = $completolong->getCompletoLong();
        $this->_view->long = $paginador->paginar($completolong->getCompletoLong(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'completolong/index');
        $this->_view->titulo = "Tablas Completas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Tablas Completas';
        $this->_view->render('index', 'longboard/completos');
    }
       
     public function verProducto($id){
        $completolong = $this->loadModel("completolong");
        $this->_view->long = $completolong->getLongCompleta($this->filtrarInt($id));
        $this->_view->titulo = "Tabla Completa";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'completolong">Completa</a> >> Tabla Completa';
        $this->_view->render('verProducto', 'completos');
    }
}
