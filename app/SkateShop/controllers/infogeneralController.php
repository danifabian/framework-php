<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of infogeneralController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class infogeneralController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    //No se utilizara
    public function index(){}
    
    public function contacto(){
        $this->_view->titulo = 'Contacto';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Contacto';
        $this->_view->render('contacto');
    }
    
    public function envio(){
        $this->_view->titulo = 'Método de envío';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Método de envío';
        $this->_view->render('envio');
    }
    
    public function compra(){
        $this->_view->titulo = 'Metodo de compra';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Método de compra';
        $this->_view->render('compra');
    }
    
    public function cambios(){
        $this->_view->titulo = 'Cambios/Devoluciones';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Cambios/Devoluciones';
        $this->_view->render('cambios');
    }
    
    
    public function sobrenosotros(){
        $this->_view->titulo = 'Sobre nosotros';       
        $this->_view->hook = 'Sobre nosotros';
        $this->_view->render('sobrenosotros');
    }
    
    public function dondeestamos(){
        $this->_view->titulo = 'Donde Estamos';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Donde Estamos';
        $this->_view->render('dondeestamos');
    }
    
    public function politicapriv(){
        $this->_view->titulo = 'Política de Privacidad';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Política de Privacidad';
        $this->_view->render('politicaprivacidad');
    }
    
    public function politicaCookies(){
        $this->_view->titulo = 'Política de Cookies';       
        $this->_view->hook = '<a href="' . BASE_URL . '">Inicio</a> >> Política de Cookies';
        $this->_view->render('politicacookies');
    }
}
