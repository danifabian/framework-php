<?php

class tornillosController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $torni = $this->loadModel("tornillos");
        #$this->_view->torni = $torni->getTornillos();
        $this->_view->torni = $paginador->paginar($torni->getTornillos(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'tornillos/index');
        $this->_view->titulo = "Accesorios Tornillos";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios Tornillos';
        $this->_view->render('index', 'accesorios/tornillos/');
    }
    
    public function verProducto($id){
        $torni = $this->loadModel("tornillos");
        $this->_view->torni = $torni->getTornillo($this->filtrarInt($id));
        $this->_view->torni = array_pop($this->_view->torni);
        $this->_view->titulo = $this->_view->torni['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'tornillos">Tornillos</a> >> ' . $this->_view->torni['modelo'];
        $this->_view->render('verProducto', 'tornillo');
    }
}