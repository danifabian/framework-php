<?php

class ruedaslongController extends Controller {
    
    public function __construct() {
        parent::__construct();
    }
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $ruedas = $this->loadModel("ruedaslong");
        #$this->_view->ruedas = $ruedas->getRuedasLong();
        $this->_view->ruedas = $paginador->paginar($ruedas->getRuedasLong(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'ruedaslong/index');
        $this->_view->titulo = "Ruedas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Ruedas';
        $this->_view->render('index', 'longboard/ruedas');
    }
    
    public function verProducto($id){
        $ruedas = $this->loadModel("ruedaslong");
        $this->_view->ruedas = $ruedas->getRuedaLong($this->filtrarInt($id));
        $this->_view->ruedas = array_pop($this->_view->ruedas);
        $this->_view->titulo = $this->_view->ruedas['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'ruedaslong">Ruedas</a> >> ' . $this->_view->ruedas['modelo'];
        $this->_view->render('verProducto', 'ruedas');
    }
}

