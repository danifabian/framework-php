<?php

class marcasController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $marca = $this->loadModel("marcas");
        #$this->_view->marca = $marca->getMarcas();
        $this->_view->marca = $paginador->paginar($marca->getMarcas(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'marcas/index');
        $this->_view->titulo = "Marcas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Marcas';
        $this->_view->render('index', 'marcas');
    }
    
    public function verMarca($id,$pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $marca = $this->loadModel("marcas");
        $this->_view->marca = $paginador->paginar($marca->getMarca($this->filtrarInt($id)), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'marcas/vermarca');
        //$this->_view->marca = $marca->getMarca($this->filtrarInt($id));
        $this->_view->titulo = "Marcas";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'marcas">Marcas</a>';
        $this->_view->render('vermarca', 'marcas');
    }
    
    public function verProducto($id){
        $marca = $this->loadModel("marcas");
        $this->_view->marca = $marca->getProducto($this->filtrarInt($id));
        $this->_view->marca = array_pop($this->_view->marca);
        $this->_view->titulo = $this->_view->marca['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'marcas">Marcas</a> >> <a href="'.BASE_URL.'marcas/verMarca/'.$this->_view->marca['id_marca'].'">Productos</a> >>' . $this->_view->marca['modelo'];
        $this->_view->render('verProducto', 'marcas');
    }
}