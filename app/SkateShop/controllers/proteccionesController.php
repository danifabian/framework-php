<?php

class proteccionesController extends Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($pagina = false){
        if(!$this->filtrarInt($pagina)){
            $pagina = false;
        }
        else{
            $pagina = (int) $pagina;
        }
        $this->getLibrary('paginador');
        $paginador = new Paginador();
        $prote = $this->loadModel("protecciones");
        #$this->_view->prote = $prote->getProtecciones();
        $this->_view->prote = $paginador->paginar($prote->getProtecciones(), $pagina);
        $this->_view->paginacion = $paginador->getView('paginador', 'protecciones/index');
        $this->_view->titulo = "Accesorios Protecciones";
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> Accesorios Protecciones';
        $this->_view->render('index', 'accesorios/lija/');
    }
    
    public function verProducto($id){
        $prote = $this->loadModel("protecciones");
        $this->_view->prote = $prote->getProteccion($this->filtrarInt($id));
        $this->_view->prote = array_pop($this->_view->prote);
        $this->_view->titulo = $this->_view->prote['modelo'];
        $this->_view->hook = '<a href="'.BASE_URL.'">Inicio</a> >> <a href="'. BASE_URL . 'protecciones">Protecciones</a> >> ' . $this->_view->prote['modelo'];
        $this->_view->render('verProducto', 'proteccion');
    }
}