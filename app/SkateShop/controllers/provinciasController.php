<?php

class provinciasController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        header("Content-Type: text/json");
        $mod = $this->loadModel('provincias');
        $provincias = $mod->getProvincias();
        print_r(json_encode($provincias));
    }
    
}