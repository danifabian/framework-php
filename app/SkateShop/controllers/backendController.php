<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of backendController
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class backendController extends Controller{
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
        
        if(Session::get('userAuth') && (Session::get('level') == 'admin' || Session::get('level') == 'gestor' )){
            $this->redireccionar('adminpanel');
        }
        elseif(Session::get('userAuth') && Session::get('level') == 'usuario'){}
        $this->_view->titulo = 'Backend';
        $this->_view->render('index');
    }
    
    public function login(){
        $users = $this->loadModel('login');
        
        if($this->getPostParam('login-btn')){//Existe envio de formulario
            if(!$this->validarEmail($this->getPostParam('email-login'))){
                $this->_view->_error = 'No ha introducido su correo';
                $this->_view->render('index');
            }
            $user = $users->login($this->getPostParam('email-login'), $this->mysql_escape_mimic('password-login'));
            print_r($user);
            
            if($user != null){
                Session::set('userAuth', true);
                Session::set('id_clie', $user['id_cliente']);
                Session::set('username', $user['username']);
                Session::set('email', $user['email']);
                Session::set('tiempo', time());
                Session::set('level', $user['role']);
                if($user['role'] == 'usuario'){//a iniciado un usuario comun
                    $this->redireccionar();
                }
                else {
                    $this->redireccionar('adminpanel');
                }
            }
            else{
                $this->_view->_error = 'Usuario o contraseña no existe';
                $this->_view->render('index');
            }
        }
    }
}
