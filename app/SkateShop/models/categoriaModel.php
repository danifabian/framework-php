<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categoriaModel
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class categoriaModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function getCategorias(){
        $cat = $this->_db->query(
            "SELECT * FROM categorias;"
            );
        return $cat->fetchAll();
    }
}
