<?php

class marcasModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getMarcas(){
        $marca = $this->_db->query("select id_marca,nombre,img_url "
                    . "from marcas;");
        return $marca->fetchAll();
    }
    
    public function getSoloMarca($id){
        $marca = $this->_db->query("SELECT nombre "
                . "FROM marcas "
                . "WHERE id_marca = $id;");
        return $marca->fetch();
    }
    
    public function getMarca($id){
        $marca = $this->_db->query("select p.id_producto, p.id_marca,m.nombre as marca, convert (p.fecha_ingreso, date) as fecha_ingreso, p.descripcion, "
                    . "p.stock, p.precio, p.modelo,p.dimensiones, p.color, p.img_url, p.destacado, p.tipo, p.img_url "
                    . "from productos p, marcas m "
                    . "where p.id_marca=m.id_marca and p.id_marca=".$id.";");
        return $marca->fetchAll();
    }
    
    
    public function getProducto($id){
        $marca = $this->_db->query("select p.id_producto,p.id_marca, m.nombre as marca, convert (p.fecha_ingreso, date) as fecha_ingreso, p.descripcion, "
                    . "p.stock, p.precio, p.modelo,p.dimensiones, p.color, p.img_url, p.destacado, p.tipo, p.id_marca,p.img_url "
                    . "from productos p, marcas m "
                    . "where p.id_marca=m.id_marca and p.id_producto=".$id.";");
        return $marca->fetchAll();
    }
}