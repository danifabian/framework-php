<?php

/**
 * Description of registroModel
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class registroModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function registro(){
        //prepare()->execute
        //Hash::getHash('sha1', $password, HASH_KEY)
    }
    
    
    public function getIdClie($id){
        $data = $this->_db->query("SELECT id_cliente "
                . "FROM clientes "
                . "WHERE id_cliente = '" . $id . "';");
        if($data->fetch())
            return true;
        return false;
    }
    public function getIdClieUser($username){
        $data = $this->_db->query("SELECT id_cliente "
                . "FROM usuarios "
                . "WHERE username = '" . $username . "';");
        if($data->fetch())
            return true;
        return false;
    }
    
    public function getUsername($username){
        $data = $this->_db->query("SELECT username "
                . "FROM usuarios "
                . "WHERE username = '" . $username . "';");
        if($data->fetch())
            return true;
        return false;
    }
    
    public function getEmail($email){
        $data = $this->_db->query("SELECT email "
                . "FROM clientes "
                . "WHERE email = '" . $email . "';");
        
        if($data->fetch())
            return true;
        
        return false;
    }
    
    public function registrarCliente($nombre, $apellidos, $direccion, $pueblo, $fecha, $email){
        $this->_db->prepare("INSERT INTO clientes (id_pueblo, nombre, apellidos, direccion, fecha_nacimiento, fecha_registro, email)"
                . "VALUES (:id_pueblo, :nombre, :apellidos, :direccion, :fecha_nacimiento, now(),:email);")
                ->execute(array(
                    ':id_pueblo' => $pueblo,
                    ':nombre' => $nombre,
                    ':apellidos' => $apellidos,
                    ':direccion' => $direccion,
                    ':fecha_nacimiento' => $fecha,
                    ':email' => $email
                ));
        
        //$id_cliente = $this->_db->query("SELECT LAST_INSERT_ID();");
        return $this->_db->query("SELECT LAST_INSERT_ID();")->fetch();
    }
    
    public function registrarTelefono($id_cliente, $tel){
        
        $this->_db->prepare("INSERT INTO telefonos (id_cliente,numero)"
                . "VALUES (:id_cliente, :numero);")
                ->execute(array(
                    ':id_cliente' => $id_cliente,
                    ':numero' => $tel,
                ));
    }
    public function registrarUsuario($username, $id_cliente, $pass, $role){
        $this->_db->prepare("INSERT INTO usuarios (username, id_cliente, pass, role)"
                . "VALUES (:username, :id_cliente, :pass, :role);")
            ->execute(array(
                ':username' => $username,
                ':id_cliente' => $id_cliente,
                ':pass' => Hash::getHash(HASH_ALGORITMO, $pass, HASH_KEY),
                ':role' => $role
            ));
    }
}
