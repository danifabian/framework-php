<?php

class completolongModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function getCompletoLong(){
        $completolong = $this->_db->query("select p.id_producto, convert(p.fecha_ingreso,date) as fecha_ingreso, "
            . "p.descripcion, p.stock, p.precio, p.modelo, m.nombre as marca, "
            . "p.dimensiones, p.peso, p.concavo, p.color, p.img_url, p.tipo "
            . "from productos p, tablas_completas tp, marcas m "
            . "where p.id_producto=tp.id_tabla and tp.id_items_tabla=p.id_producto and p.id_marca=m.id_marca "
            . "and p.id_categoria=2");
        return $completolong->fetchAll();
    }
    
    public function getLongCompleta($id){
        $completolong = $this->_db->query("select p.id_producto as completo, h.id_producto, "
            . "h.descripcion, h.tipo, h.precio, h.color, h.dimensiones, h.peso, "
            . "h.modelo, convert(h.fecha_ingreso,date) as fecha_ingreso, h.img_url, "
            . "h.stock, m.nombre as marca "
            . "from productos p, tablas_completas tp, productos h, marcas m "
            . "where p.id_producto=tp.id_tabla and tp.id_items_tabla=h.id_producto "
            . "and p.id_marca=m.id_marca and p.id_producto=".$id."");
        return $completolong->fetchAll();
    }
}