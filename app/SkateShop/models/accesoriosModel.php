<?php

class accesoriosModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getAccesorios(){
        $acce = $this->_db->query("select p.id_producto, m.nombre as marca, convert (p.fecha_ingreso, date) as fecha_ingreso, p.descripcion, p.stock, p.precio, "
                . "p.modelo,p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and (p.tipo='cordones' or p.tipo='tornillo' or p.tipo='lija' or p.tipo='proteccion' or p.tipo='goma' or p.tipo='herramienta')");
        return $acce->fetchAll();
    }
    
    public function getAccesorio($id){
        $acce = $this->_db->query("select p.id_producto, m.nombre as marca, convert (p.fecha_ingreso, date) as fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and p.id_producto=".$id.";'");
        return $acce->fetchAll();
    }
        
}