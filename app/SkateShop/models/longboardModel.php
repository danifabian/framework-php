<?php

class longboardModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    public function getLongboard(){
        $long = $this->_db->query("select id_producto, m.nombre as marca, c.nombre as categoria, p.fecha_ingreso, p.descripcion, p.stock, p.modelo, p.dimensiones, p.peso, p.color, p.img_url, p.tipo, p.precio "
                                . "from productos p, marcas m, categorias c "
                                . "where p.id_marca=m.id_marca and p.id_categoria=c.id_categoria and p.id_categoria=2"
                );
        return $long->fetchAll();
    }
    
    public function getLongProducto($id){
        $long = $this->_db->query("select p.id_producto, m.nombre as marca, c.nombre as categoria, convert(p.fecha_ingreso,date) as fecha_ingreso, p.descripcion, p.stock, p.modelo, p.dimensiones, p.peso, p.color, p.img_url, p.tipo, p.precio "
                                . "from productos p, marcas m, categorias c "
                                . "where p.id_marca=m.id_marca and p.id_categoria=c.id_categoria and p.id_producto=". $id ."");
        
        return $long->fetchAll();
    }
}