<?php

class ejesboardModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getEjesBoard(){
        $ejes = $this->_db->query("select p.id_producto, m.nombre as marca, c.nombre as categoria, convert(p.fecha_ingreso,date) as fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.peso, p.concavo, p.color, p.img_url, p.tipo "
                . "from productos p, marcas m, categorias c "
                . "where p.id_marca=m.id_marca and p.id_categoria=c.id_categoria and p.tipo='ejes' and p.id_categoria=1;");
        return $ejes->fetchAll();
    }
    
    public function getEjeBoard($id){
        $ejes = $this->_db->query("select p.id_producto, m.nombre as marca, c.nombre as categoria, convert(p.fecha_ingreso,date) as fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.peso, p.concavo, p.color, p.img_url, p.tipo "
                . "from productos p, marcas m, categorias c "
                . "where p.id_marca=m.id_marca and p.id_categoria=c.id_categoria and p.tipo='ejes' and p.id_categoria=1 and p.id_producto=".$id.";");
        return $ejes->fetchAll();
    }
        
}
