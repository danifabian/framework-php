<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of usuariosModel
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class usuariosModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function getUser($id, $username, $email){
        $data = $this->_db->query(
            "SELECT c.*,u.*, p.nombre as pueblo, pr.nombre as provincia, p.cod_postal, t.numero "
            . "FROM clientes c, usuarios u, pueblos p, provincias pr, telefonos t "
            . "WHERE c.id_cliente = u.id_cliente AND c.id_cliente = $id "
                . "AND c.email = '$email' AND u.username = '$username' "
                . "AND c.id_pueblo = p.id_pueblo AND p.id_provincia = pr.id_provincia ;");
        return $data->fetch();
    }
    
    public function getPerfil($id){
        $clie = $this->_db->query("select c.id_cliente, c.nombre, c.apellidos, c.fecha_nacimiento, c.fecha_registro, c.email, c.direccion, u.username, p.nombre as pueblo, pr.nombre as provincia, p.cod_postal "
            . "from clientes c, usuarios u, provincias pr, pueblos p "
            . "where c.id_cliente = u.id_cliente and c.id_pueblo = p.id_pueblo and p.id_provincia = pr.id_provincia and c.id_cliente = $id");
        return $clie->fetchAll();
    }
    
    public function updatePerfil($id,$username, $email, $direccion, $provincia, $pueblo, $cod_postal){
        $this->_db->prepare(
            "UPDATE clientes "
                . "SET email=:email, direccion=:direccion "
                . "WHERE id_cliente = :id")
            ->execute(array(
                ':id' => $id,
                ':email' => $email,
                ':direccion' => $direccion
            ));
    }
    
}
