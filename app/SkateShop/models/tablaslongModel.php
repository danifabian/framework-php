<?php

class tablaslongModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function getTablasLong(){
        $tablas = $this->_db->query("select p.id_producto, m.nombre as marca, p.modelo, p.stock, p.img_url, p.peso, p.concavo, p.color, p.dimensiones, p.descripcion,p.tipo, p.precio "
                                    . "from productos p, marcas m "
                                    . "where p.id_marca=m.id_marca and p.tipo='tabla' and p.id_categoria=2");
        return $tablas->fetchAll();
    }
    
    public function getTablaLong($id){
        $tablas = $this->_db->query("select p.id_producto, m.nombre as marca, p.modelo, p.stock, p.img_url, p.peso, "
                                    . "p.concavo, p.color, p.dimensiones, p.descripcion,p.tipo, p.precio, convert(p.fecha_ingreso,date) as fecha_ingreso "
                                    . "from productos p, marcas m "
                                    . "where p.id_marca=m.id_marca and p.tipo='tabla' "
                                    . "and p.id_categoria=2 and id_producto=".$id."");
        return $tablas->fetchAll();
    }
}
