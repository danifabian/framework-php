<?php

class cordonesModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getCordones(){
        $cordo = $this->_db->query("select p.id_producto, m.nombre as marca, p.fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and p.tipo='cordones'");
        return $cordo->fetchAll();
    }
    
    public function getCordon($id){
        $cordo = $this->_db->query("select p.id_producto, m.nombre as marca, p.fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and p.id_producto=".$id.";'");
        return $cordo->fetchAll();
    }
        
}