<?php

class facturasModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function issetCodProd($cod_prod){
        $codigo = $this->_db->query("select codigo_pedido "
            . "from facturas "
            . "where codigo_pedido='".$cod_prod."'");
        if($codigo->fetch())
            return true;
        return false;
    }
    
    public function setFactura($id, $cod_ped, $dto){
        $this->_db->prepare(
            "INSERT INTO facturas (id_cliente, codigo_pedido, fecha, dto) "
                . "VALUES (:id_cliente, :codigo_pedido, now(), :dto)")
        ->execute(
            array(
                ':id_cliente' => $id,
                ':codigo_pedido' => $cod_ped,
                ':dto' => $dto
            )
        );
        $id_fact = $this->_db->query(
            "SELECT id_factura "
            . "FROM facturas "
            . "ORDER BY 1 DESC "
            . "LIMIT 1;");
        return $id_fact->fetch();
        
    }
    
    public function getFacturas($id){
        $clie = $this->_db->query("select f.id_factura, f.codigo_pedido, convert(f.fecha, date) as fecha "
                . "from facturas f, clientes c "
                . "where f.id_cliente=c.id_cliente and c.email='".$id."';'");
        return $clie->fetchAll();
    }
    
    public function getFactura($codFac){
        $pdf = $this->_db->query("select f.codigo_pedido, f.id_factura, lf.id_linea, lf.id_producto, p.descripcion,lf.cant, "
            . "m.nombre as marca, p.precio, p.modelo, p.dimensiones, p.tipo, convert (f.fecha, date) as fecha_pedido,p.img_url, "
            . "lf.cant, c.nombre, c.apellidos, c.direccion, c.email, t.numero, pu.nombre as pueblo, pr.nombre as provincia, pu.cod_postal "
            . "from  facturas f, lineas_fac lf, productos p, marcas m, clientes c, telefonos t, pueblos pu, provincias pr "
            . "where c.id_cliente = f.id_cliente AND f.id_factura=lf.id_factura and lf.id_producto=p.id_producto and m.id_marca=p.id_marca and c.id_pueblo=pu.id_pueblo and "
            . "pu.id_provincia=pr.id_provincia and f.codigo_pedido='".$codFac."'"
            . "group by lf.id_linea;");
    
        return $pdf->fetchAll();
    }
}
