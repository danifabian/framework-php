<?php

class lineasfactModel extends Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function updateCantidad($producto,$carrito){
        $this->_db->prepare(
            "UPDATE productos "
                . "SET stock=:stock "
                . "WHERE id_producto = :id")
            ->execute(array(
                ':id' => $carrito['id'],
                ':stock' => $producto['stock']-$carrito['cantidad'],
            ));
    }
    public function setLineasFact($id_fact, $carrito, $dto, $linea){
        echo $dto. '- ' . $id_fact;
        $this->_db->prepare(
            "INSERT INTO lineas_fac (id_linea, id_factura, id_producto, cant, dto, precio) "
                . "VALUES (:id_linea, :id_factura, :id_producto, :cant, :dto, :precio)")
        ->execute(
            array(
                ':id_linea' => $linea,
                ':id_factura' => $id_fact,
                ':id_producto' => $carrito['id'],
                ':cant' => $carrito['cantidad'],
                ':dto' => $dto,
                ':precio' => $carrito['precio']
            )
        );
        /*if(sizeof($carrito) > 1){//Hay + de un elemento en el carrito
            foreach($carrito as $itemkey){
                echo $itemkey['id'].' - '.$itemkey['cantidad'].' - '.$itemkey['precio'];
                $this->_db->prepare(
                    "INSERT INTO lineas_fac (id_factura, id_producto, cant, dto, precio) "
                        . "VALUES (:id_factura, :id_producto, :cant, :dto, :precio)")
                ->execute(
                    array(
                        ':id_factura' => $id_fact,
                        ':id_producto' => $itemkey['id'],
                        ':cant' => $itemkey['cantidad'],
                        ':dto' => $dto,
                        ':precio' => $itemkey['precio']
                    )
                );
                echo '<pre>';
                print_r($itemkey);
            }
        }
        else{//
            
            /*$this->_db->prepare(
            "INSERT INTO lineas_fac (id_factura, id_producto, cant, dto, precio) "
                . "VALUES (:id_factura, :id_producto, :cant, :dto, :precio)")
            ->execute(
                array(
                    ':id_factura' => $id,
                    ':id_producto' => $cod_ped,
                    ':cant' => '',
                    ':dto' => $dto,
                    ':precio' => ''
                )
            );*/
        //}
    }
}