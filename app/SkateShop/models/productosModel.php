<?php

class productosModel extends Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getProductoId($carrito){
        $productos = $this->_db->query("select * " 
                . "from productos "
                . "where id_producto=".$carrito['id'].";");
        return $productos->fetch();
    }
    
    public function getProductosInicio(){
        $productos = $this->_db->query("SELECT p.id_producto, m.nombre as marca, p.modelo, "
                    . "c.nombre as categoria, fecha_ingreso, p.stock, p.precio, p.img_url, "
                    . "p.dimensiones, p.peso, p.concavo, p.color, p.tipo, p.destacado "
                . "FROM productos p, marcas m, categorias c "
                . "WHERE m.id_marca = p.id_marca AND c.id_categoria = p.id_categoria "
                    . "and destacado = 1 LIMIT 8;");
        return $productos->fetchAll();
    }
    
    public function listadoProductos(){
        $productos = $this->_db->query("SELECT p.id_producto, m.nombre as marca, p.modelo, "
                    . "c.nombre as categoria, p.fecha_ingreso, p.stock, p.precio, p.img_url, "
                    . "p.dimensiones, p.peso, p.concavo, p.color, p.tipo, p.destacado "
                . "FROM productos p, marcas m, categorias c "
                . "WHERE m.id_marca = p.id_marca AND c.id_categoria = p.id_categoria "
                . "ORDER BY p.fecha_ingreso DESC;");
        return $productos->fetchAll();
    }
    
    public function getProducto($id){
        $productos = $this->_db->query("SELECT p.id_producto, m.nombre as marca, m.id_marca, p.modelo, "
                    . "c.nombre as categoria, c.id_categoria, p.fecha_ingreso, p.stock, p.precio, p.img_url, "
                    . "p.dimensiones, p.peso, p.concavo, p.color, p.tipo, p.destacado "
                . "FROM productos p, marcas m, categorias c "
                . "WHERE m.id_marca = p.id_marca AND c.id_categoria = p.id_categoria "
                . "AND p.id_producto = $id;");
        return $productos->fetch();
    }
    
    //Admin panel actions
    //Eliminar producto
    public function deleteItem($id){
        $this->_db->query("DELETE FROM productos "
                . "WHERE id_producto = $id;");
    }
    
    public function buscadorProducto($valor){
        $productos = $this->_db->query("select p.*, m.nombre as marca "
            . "from productos p, marcas m "
            . "where p.id_marca=m.id_marca and "
            . "(m.nombre like '%".$valor."%' or "
            . "p.descripcion like '%".$valor."%' or "
            . "p.modelo like '%".$valor."%')"
            . "group by p.id_producto;");
        return $productos->fetchAll();
    }
    public function createProd($id_marca, $id_categoria, $fecha_ingreso, $descrip,
        $stock, $precio, $modelo, $dimension, $peso, $concavo,
        $color, $img_url, $destacado, $tipo){
        $this->_db->prepare("INSERT INTO productos(id_marca, id_categoria, fecha_ingreso, descripcion, "
                . "stock, precio, modelo, dimensiones, peso, concavo, color, img_url, destacado, tipo)"
                . "VALUES (:id_marca, :id_categoria, :fecha_ingreso, :descripcion, "
                    . ":stock, :precio, :modelo, :dimensiones, :peso, :concavo, :color, :img_url, :destacado, "
                    . ":tipo)")
        ->execute(array(
                ':id_marca' => $id_marca,
                ':id_categoria' => $id_categoria,
                ':fecha_ingreso' => $fecha_ingreso,
                ':descripcion' => $descrip,
                ':stock' => $stock,
                ':precio' => $precio,
                ':modelo' => $modelo,
                ':dimensiones' => $dimension,
                ':peso' => $peso,
                ':concavo' => $concavo,
                ':color' => $color,
                ':img_url' => $img_url,
                ':destacado' => $destacado,
                ':tipo' => $tipo
            )
        );
    }
    public function updateProducto($id_producto, $id_marca, $stock, $precio, $color, $destacado, $img_url){
        $this->_db->prepare("UPDATE productos "
                . "SET id_marca=:id_marca, stock=:stock, precio=:precio, color=:color, destacado=:destacado, img_url=:img_url "
                . "WHERE id_producto = :id_producto")
            ->execute(array(
                ':id_marca' => $id_marca,
                ':id_producto' => $id_producto,
                ':stock' => $stock,
                ':precio' => $precio,
                ':color' => $color,
                ':destacado' => $destacado,
                ':img_url' => $img_url
                
            ));
    }
}