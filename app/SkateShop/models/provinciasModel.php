<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of provinciasModel
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class provinciasModel extends Model{
   public function __construct() {
       parent::__construct();
   }
   
   public function getProvincias(){
       $provincias = $this->_db->query("SELECT * "
               . "FROM provincias");
       return $provincias->fetchAll();
   }
}
