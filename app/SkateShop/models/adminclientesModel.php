<?php

class adminclientesModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getClientes(){
        $clie = $this->_db->query("select distinct c.id_cliente, c.id_pueblo, c.nombre, c.apellidos, c.direccion, c.fecha_nacimiento, "
                . "c.fecha_registro, c.email, u.username,u.role "
                . "from clientes c LEFT JOIN  usuarios u ON c.id_cliente=u.id_cliente "
                . "group by  c.email "
                . "order by c.id_cliente");
        return $clie->fetchAll();
    }
    
    
}