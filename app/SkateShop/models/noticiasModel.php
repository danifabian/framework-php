<?php

class noticiasModel extends Model {
    
    public function __construct() {
        parent::__construct();
    }
    /* devolver las tres ultimas noticias insertadas
     * para mostrar en la pagina de inicio
     */
    public function getNoticiasInicio(){
        $noticias = $this->_db->query("SELECT id_noticia, username, titulo, cuerpo, fecha_modificacion, img_url "
                . "FROM noticias "
                . "ORDER BY fecha_modificacion DESC limit 3");
        
        return $noticias->fetchAll();
    }
    
    /* Devuelve todas las noticias ordenadas por fecha de moficiacion
     * (se devuelven todas las noticias y una clase paginador se encarga de gestionar 
     * la cantidad de datos en paginas)
     */
    public function getNoticias(){
        $noticias = $this->_db->query("SELECT id_noticia, username, titulo, cuerpo, img_url, fecha_modificacion "
                . "FROM noticias "
                . "ORDER BY fecha_modificacion DESC");
        
        return $noticias->fetchAll();
    }
    
    /*
     * Devuelve una notícia en concreto
     */
    public function getNoticia($id){
        $noticias = $this->_db->query("SELECT id_noticia, username, titulo, cuerpo, fecha_modificacion, img_url "
                . "FROM noticias "
                . "WHERE id_noticia =" . $id .";");
        
        return $noticias->fetchAll();
    }
    
    /*
     * Apartado de administrador
     * Eliminar una notícia
     */
    public function deleteNoticia($id){
        $this->_db->prepare(
            "DELETE FROM noticias "
            . "WHERE id_noticia = :id_noticia")
            ->execute(array(
                ':id_noticia' => $id
        ));
    }
    /*
     * Crear una notícia
     */
    public function createNoticia($username, $titulo, $cuerpo, $img_url){
        $this->_db->prepare(
            "INSERT INTO noticias (username, titulo, cuerpo, fecha_registro, fecha_modificacion, img_url) "
                . "VALUES (:username, :titulo, :cuerpo, now(), now(), :img_url)")
            ->execute(array(
                ':username' => $username,
                ':titulo' => $titulo,
                ':cuerpo' => $cuerpo,
                ':img_url' => $img_url
            ));
    }
    /*
     * Modifica notícia
     */
    public function updateNoticia($id, $username, $titulo, $cuerpo, $img_url){
        //echo $id . ' -  ' . $username . ' -  ' . $titulo . ' -  ' . $cuerpo . ' -  ' . $img_url;
        if(is_null($img_url)){//Actualizar contenido
            $this->_db->prepare(
            "UPDATE noticias "
                . "SET username=:username, titulo=:titulo, cuerpo=:cuerpo, fecha_modificacion=now() "
                . "WHERE id_noticia = :id")
            ->execute(array(
                ':id' => $id,
                ':username' => $username,
                ':titulo' => $titulo,
                ':cuerpo' => $cuerpo
            ));
        }
        else{//Actualizar contenido + imagen 
            $this->_db->prepare(
            "UPDATE noticias "
                . "SET username=:username, titulo=:titulo, cuerpo=:cuerpo, fecha_modificacion=now(), img_url=:img_url "
                . "WHERE id_noticia = :id")
            ->execute(array(
                ':id' => $id,
                ':username' => $username,
                ':titulo' => $titulo,
                ':cuerpo' => $cuerpo,
                ':img_url' => $img_url
            ));
        }
    }
}