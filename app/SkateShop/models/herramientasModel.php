<?php

class herramientasModel extends Model {
    public function __construct() {
        parent::__construct();
    }
    public function getHerramientas(){
        $herra = $this->_db->query("select p.id_producto, m.nombre as marca, p.fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and p.tipo='herramienta'");
        return $herra->fetchAll();
    }
    
    public function getHerramienta($id){
        $herra = $this->_db->query("select p.id_producto, m.nombre as marca, p.fecha_ingreso, p.descripcion, p.stock, p.precio, p.modelo, "
                . "p.dimensiones, p.color, p.img_url, p.destacado, p.tipo "
                . "from productos p, marcas m "
                . "where p.id_marca=m.id_marca and p.id_producto=".$id.";'");
        return $herra->fetchAll();
    }
        
}