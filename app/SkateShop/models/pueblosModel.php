<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pueblos
 *
 * @author Daniel Fabián <dafaos90@gmail.com>
 */
class pueblosModel extends Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function getPueblos($id){
        $pueblos = $this->_db->query(
                "SELECT p.id_pueblo, p.nombre, p.cod_postal "
                . "FROM pueblos p, provincias po "
                . "WHERE p.id_provincia = po.id_provincia AND "
                . "po.id_provincia = $id"
                );
        
        return $pueblos->fetchAll();
    }
    
    public function getCp($id){
        $cp = $this->_db->query("SELECT cod_postal "
                . "FROM pueblos "
                . "WHERE id_pueblo = $id;");
        return $cp->fetch();
    }
}
