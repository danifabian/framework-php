/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
     * Gestionar carrito de compra
     */
    var carritoModule = ({
        init : (function(){//Iniciar sesion carrito
            $.ajax({
                url: "carrito/init",
                type: 'POST',
                async: true,
                error: function(response){
                    //No deberia existir ningun error
                    console.log("Error init: " + response);
                },
                success: function(data){
                    //Carrito creado o vacio
                    if(data == false){
                        $('.total-items').html('0');
                        $('.total-price').html('0.00');
                    }
                    else{
                        carritoModule.pintarCarrito(data);
                    }
                }
            });
        }),
        addItem : (function(item){//Añadir item al carrito
            $.ajax({
                url: "carrito/addItemCarrito", // Url a la que se envía lapetición
                type: 'POST', // Método para la petición
                dataType: 'json',
                data: {'json' : JSON.stringify(item)},
                async: true, // Permite hacer que las peticiones ajax sean síncronas.
                beforeSend: function(){
                    console.log("Antes de enviar ajax");
                    console.log('Archivo JSON: ' + item);
                }, // Se ejecuta antes de enviar la petición.
                cache: false, // Se le pide al navegador que no use caché si se establece a false.
                error: function(response){
                    console.log("Hubo algun tipo de error");
                    console.log(response);
                }, // Se ejecuta en caso de error de conexión o del servidor
                success: function(data){
                    //Esta linea da error en la consola. Correccion enviando desde php 
                    //indicando en la cabecera el envio de json
                    //var data = $.parseJSON(response);//transformar json de respuesta
                    console.log('AddItem: ' + data);
                    carritoModule.pintarCarrito(data);
                }
            });
        }),
        deleteItem : (function(item){
            console.log('DeleteItem oject: ' + item);
        }),
        updateItem : (function(item){
            console.log('UpdateItem: ' + item);
        }),
        checkItem : (function(id){
            console.log('CheckItem: ' + id);
        }),
        pintarCarrito : (function(data){
            console.log("Pintar data: " + data);
            var div = $(".item-list");
            var cantTotal = 0;
            var preciototal = 0;
            var precioItem = 0;
            div.empty();
            div.append("<table>");
            $.each(data, function(i, item){
                div.append("<tr>");
                    div.append("<td style='display: none'>" + item.id + "</td>");
                    div.append("<td>Marca: " + item.marca + "</td>");
                    div.append("<td>Modelo: " + item.modelo +"</td>");
                    div.append("<td>Cantidad: " + parseInt(item.cantidad) +"</td>");
                    cantTotal += item.cantidad;
                    preciototal += item.precio * item.cantidad;
                    precioItem = item.precio * item.cantidad;
                    div.append("<td>Precio: " + precioItem + " €</td>");
                    div.append("<td><a href=''><img src='app/_layout/default/img/general/ic_edit.png' alt='edit' class='ico-small' /></a></td>");
                    div.append("<td><a href='javascript: void(0);' class='eliminar-item' ><img src='app/_layout/default/img/general/ic_clear.png' alt='" + item.id + "' class='ico-small' /></a></td>");
                div.append("</tr>");
            });
            
            div.append("</table>");
        })
    });
    //carritoModule.init();
    
    /*
     * Clicar en añadir un item en el carrito.
     * Enviar paquete json con la estructura del elemento a añadir
     * al controlador carrito metodo additemCarrito
     * El controlador decodifica el paquete json, 
     * Añadir elemento nuevo en la sesion carrito
     * Retornar json / array con todo el carrito o vista con la 
     * presentacion carrito
     * Pintar respuesta controlador
     */
    //$(document).on('click','a.add-item',function (event) {
    $('.add-item').click(function(event){
        event.preventDefault();
        console.log('Add item');
        //Obtener div contenedor del producto clicado
        var parent = $(this).parent().parent();
        //Obtener objeto con todos los hijos del padre
        var child = parent.children();
        //Nuestro array de datos. Almacenara todo el contenido
        var data = [];
        var json;
        //Recorrer todos los hijos
        child.each(function(index, el) {
            //Saber si existen hijos span
            var span = $(el).find('span');
            if(span.length > 0){
                //Saber si existe algun select dentro de algun span
                var select = $(span).find('select');
                if(select.length > 0)
                    data[index] = $(select).val();//guardar valor de option
                else
                    data[index] = $(span).text();//guardar contenido texto
            }
            else{//Si no hay span guardamos el texto actual
                data[index] = $(el).text();//guardar contenido texto
            }
        });
        //Eliminar los dos ultimos elementos del array data
        //ya que son los dos ultimos links del div de producto
        data.pop();
        data.pop();
        json = {///Generar json con la informacion a enviar
            'id' : data[0],
            'marca' : data[1],
            'modelo' : data[2],
            'precio' : parseFloat(data[3]),//Parsear info string a valor numerico
            'stock' : parseInt(data[4]),
            'cantidad' : parseInt(data[5])
        };
        //Comprobar si podemos retirar tantos productos como queremos
        if((json.stock-json.cantidad) >= 0){
            //Añadir item al carrito
            carritoModule.addItem(json);
        }
        else{///Mostrar mensage de warning avisando al usuario que no podemos retirar esa cantidad de stock
            console.log('No tenemos stock');
            //msgModule.error('No puede añadir tanto elementos al carrito.');
        }
    });
    
    $(document).on('click','a.eliminar-item',function (event) {
        event.preventDefault();
        var child = $(this).attr('href');
        carritoModule.deleteItem(child);
    });
    