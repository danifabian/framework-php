window.onload = function(){
    var carac_especial=":,;!|�#$%&�/()=?�+*^[]��{}~<>";
    var invalido = true;
    var numeros = "0123456789";
    var caracteres = "abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@._-";
};

function validar(valor){
    for (var i=0; i < valor.elements.length; i++){
        campo = valor.elements[i];
        if (campo.type === "text" || campo.type === "password" || campo.type === "email"){
            invalido = permitir(campo.value);
            if(invalido == false) return invalido;
        }
        if (campo.type === "number" || campo.type === "tel" ){
            invalido = numero(campo.value);
            if(invalido == false) return invalido;
        }
    }
    
    return true;
}
function numero(valor){
    
    if (valor == ""){
        return false;
    }else{
        for(var i=0, n=0; i< valor.length; i++){
            for(var j=0; j < numeros.length; j++){
                if(valor[i] == numeros[j]){
                    n++;
                }
            }
        }
        if(n == valor.length){
            return true;
        }else{
            return false;
        }
    }
    
    return true;
}
function permitir(valor){
    
    if (valor == ""){
        return false;
    }else{       
        for (var i=0; i < valor.length; i++){
            for(var j=0; j < carac_especial.length; j++){
                if (valor[i] === carac_especial[j]){
                    return false;
                }
            }
        }
    }
    return true;
}