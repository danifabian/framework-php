
$(document).ready(function() {
//(function($) {
    var SliderModule = (function(){
        /* Configuracion animationEffect
         *  slideDownUp
         *  slideFadeOutIn
         *  Si no se indica nada por defecto slideDownup
         */
        this.config = {//Configuracion basica
            tiempo : 6,//Tiempo en segundos presentacion imagen
            animationTime : 0.5,//Tiempo entre transicion de imagenes
            animationEffect : '',
            tamSlider : $('#slider').find('li').length,
            tamControls : $('.slide-control ul').find('li').length,
            currentSlide : 0,
            nextSlide : 1
        };
        
        function bucleMovimientoSlide(current, next){/* Movimiento circular del slider */
            if(next === this.config.tamSlider - 1 ){/* Mismo valor y tipo */
                this.config.nextSlide = 0;
            } else{ this.config.nextSlide++; }
            if(current === this.config.tamSlider - 1 ){
                this.config.currentSlide = 0;
            } else{ this.config.currentSlide++; }
        }
        
        function animacion(){/* Efectos animacion */
            var slider = $('#slider');
            var item = slider.find('li');
            var controlPanel = $('.slide-control ul');
            var itemControl = controlPanel.find('li');
            //Animacion 
            switch(this.config.animationEffect){
                case 'slideDownUp':
                default:
                    item.eq(this.config.currentSlide).slideUp(this.config.animationTime*1000, function(){
                        item.eq(config.currentSlide).removeClass('current-slide');
                        itemControl.eq(config.currentSlide).removeClass('current-control-slide');
                        item.eq(config.nextSlide).slideDown(config.animationTime*1000, function(){
                            item.eq(config.nextSlide).addClass('current-slide');
                            itemControl.eq(config.nextSlide).addClass('current-control-slide');
                            bucleMovimientoSlide(config.currentSlide, config.nextSlide);
                        });
                    });
                    break;
                case 'slideFadeOutIn':
                    item.eq(this.config.currentSlide).fadeOut(this.config.animationTime*1000, function(){
                        item.eq(config.currentSlide).removeClass('current-slide');
                        itemControl.eq(config.currentSlide).removeClass('current-control-slide');
                        item.eq(config.nextSlide).fadeIn(config.animationTime*1000, function(){
                            item.eq(config.nextSlide).addClass('current-slide');
                            itemControl.eq(config.nextSlide).addClass('current-control-slide');
                            bucleMovimientoSlide(config.currentSlide, config.nextSlide);
                        });
                    });
                    break;
            }
        }
        /* 
         * Cambiar posicion de la imagen segun hayamos clicado en el panel de controle
         * del slider
         */
        $('.slide-control li').click(function(){
            var indice = $('.slide-control li').index(this);
            
            //console.log(indice);
        });
        //Llamada infinita
        setInterval(animacion, this.config.tiempo*1000);
    });
    //Arrancar modulo Slider
    SliderModule();
    
    /* 
     * Gestionar cookies con js y jquery
     */
    //Objeto cookieModule
    var cookiesModule = ({
        config : ({
            c_name : 'skate_shop_privacidad',
            c_days : 7,
            c_value : 0,
            c_path : 'path=/'
        }),
        checkCookie : (function(cname){
            var cookie = cookiesModule.getCookie(cname);
            if(cookie === false){//Si no existe la cookie la creamos con su valor por defecto
                cookiesModule.setCookie(cname, cookiesModule.config.c_value, cookiesModule.config.c_days);
            }
            //Retorna la cookie
            return cookiesModule.getCookie(cname);
        }),
        setCookie : (function(cname, value, days){
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            var expires = "; expires=" + date.toGMTString();
            document.cookie = cname + '=' + value + expires + ';' + this.config.c_path;//Creacion de la cookie
        }),
        getCookie : (function(cname){
            var name = cname + '=';
            var cookies = document.cookie.split(';');
            var i = 0;
            
            for(i; i < cookies.length; i++){
                var cookie = cookies[i];
                while(cookie.charAt(0) == ' ')
                    cookie = cookie.substring(1);
                if(cookie.indexOf(name) == 0)
                    return cookie.substring(name.length, cookie.length);
            }
            //Si no encuentra la cookie devuelve false
            return false;
        }),
        deleteCookie : (function(cname){
            cookiesModule.setCookie(cname, '', -1);
        })
    });
    ///Comprobar si el navegador tiene las cookies habilitadas
    if (!navigator.cookieEnabled){
        /* En caso en que no las tenga habilitadas mostramos en ucadro de dialogo
         * avisando al usuario que debe habilitar las cookies
         * (Al crear un propio estilo para los dialogos, hay que implementar
         * mas llamadas para realizar una "copia del funcionamiento")
         */
        $( "body" ).addClass('ui-widget-overlay');
        $('.c_enabled').css('display', 'block');
        $('.c_enabled').dialog({
            open: function() { $(".ui-dialog-titlebar-close").hide(); },
            title: 'Cookies deshabilitadas',
            content: 'No tiene habilitada las Cookies en su navegador.<br>Por favor, habilítelas.',
            modal: true,
            buttons: {
              Cerrar: function() {
                $( this ).dialog( "close" );
                $("body").removeClass('ui-widget-overlay');
              }
            }
        }); 
    }else{ //Navegador tiene cookies activadas
        //Buscar si existe la cookie de privacidad
        var cookvalue = cookiesModule.checkCookie(cookiesModule.config.c_name);
        if(cookvalue == 0){
            $(".politica_cookies").css("display" , "block");
            $(".c_enabled").css("display" , "none");
        }
    }
    $('.acetpa_politica_cookies').click(function(event){
       event.preventDefault();
       cookiesModule.setCookie(cookiesModule.config.c_name, 1, cookiesModule.config.c_days);
       $(".politica_cookies").css("display" , "none");
       $(".c_enabled").css("display" , "none");
    });
    
    /*
     * Gestión del carrito
     */
    var carritoModule = ({
        config : {//Configuracion basica
            base_url : window.location.origin,
            //base_url : window.location.origin + '/~tdiw-d7/',
            urlInit : '/carrito/init',
            urlAdd : '/carrito/addItem',
            urlDelete : '/carrito/deleteItem',
            urlUpdate : '/carrito/updateItem'
        },
        autoload : (function(){//funcion de auto cargar. se ejecuta automaticamente cada vez que se recarga la pagina
            $.ajax({
                url: carritoModule.config.base_url + carritoModule.config.urlInit,
                type: 'POST',
                dataType : 'json',
                data : '',
                async: true,
                error: function(error){
                    //No deberia existir ningun error
                    console.log("Error init: " + error);
                },
                success: function(data){
                    if(data.length !== 0){//Comprobar que el objeto no este vacio
                        carritoModule.pintarCarrito(data);
                    }
                    else{
                        $(".comprar-carrito-dinamico").remove();
                        $('.total-items').html('0');
                        $('.total-price').html('0.00');
                    }
                }
            });
        }),
        addItem : (function(item){
            console.log(carritoModule.config.base_url);
            var data = JSON.stringify(item);
            $.ajax({
                url: carritoModule.config.base_url + carritoModule.config.urlAdd,
                type: 'POST', 
                dataType: 'json',
                data: {'json' : data},
                async: true, 
                cache: false, 
                error: function(response){
                    console.log("Hubo algun tipo de error");
                    console.log(response);
                }, 
                success: function(data){
                    carritoModule.pintarCarrito(data);
                }
            });
        }),
        deleteItem : (function(item){
            var data = JSON.stringify(item);
            $.ajax({
                url: carritoModule.config.base_url + carritoModule.config.urlDelete,
                type: 'POST', 
                dataType: 'json',
                data: {'json' : data},
                async: true, 
                cache: false, 
                error: function(response){
                    console.log("Hubo algun tipo de error");
                    console.log(response);
                }, 
                success: function(data){
                    carritoModule.pintarCarrito(data);
                    if(URLactual['href'] === (registroModule.config.base_url + '/carrito/mostrarcarrito')
                       || URLactual['href'] === (registroModule.config.base_url + '/carrito/mostrarCarrito')){
                       location.href = registroModule.config.base_url + '/carrito/mostrarcarrito';
                    }
                }
            });
        }),
        pintarCarrito : (function(data){
            var div = $("div.item-list");
            var contenidoTabla = "<table>";
            var cantTotal = 0;
            var preciototal = 0;
            var precioItem = 0;
            if(!jQuery.isEmptyObject(data)){
                div.empty();
                $.each(data, function(i, item){
                    contenidoTabla += "<tr>";
                        contenidoTabla += "<td style='display: none'>" + item.id + "</td>";
                        contenidoTabla += "<td>Marca: " + item.marca + "</td>";
                        contenidoTabla += "<td>Modelo: " + item.modelo +"</td>";
                        contenidoTabla += "<td>Cantidad: " + parseInt(item.cantidad) +"</td>";
                        cantTotal += item.cantidad;
                        preciototal += item.precio * item.cantidad;
                        precioItem = item.precio * item.cantidad;
                        contenidoTabla += "<td>Precio: " + precioItem.toFixed(2)  + "</td>";
                        contenidoTabla += "<td><a href='javascript: void(0);' class='actualizar-item'><img src='" + carritoModule.config.base_url + "/app/_layout/default/img/general/ic_edit.png' alt='" + item.id + "' class='ico-small' /></a></td>";
                        contenidoTabla += "<td><a href='javascript: void(0);' class='eliminar-item'><img src='" + carritoModule.config.base_url + "/app/_layout/default/img/general/ic_clear.png' alt='" + item.id + "' class='ico-small' /></a></td>";
                    contenidoTabla += "</tr>";
                });
                contenidoTabla += "</table>";
                contenidoTabla += "<a class='comprar-carrito-dinamico' href='" + carritoModule.config.base_url + "/carrito/mostrarCarrito'>Comprar</a>";
                div.append(contenidoTabla);
            }else{
                div.empty();
            }
            $('.total-items').html(cantTotal);
            $('.total-price').html(preciototal.toFixed(2));
            
        })
    });
    //Autocargar carrito 
    carritoModule.autoload();
    
    //Añadir un nuevo item al carrito
    $('.add-item').click(function(event){
        event.preventDefault();
        var parent = $(this).parent().parent();
        var child = parent.children();
        var data = [];
        
        child.each(function(index, el) {
            var span = $(el).find('span');
            if(span.length > 0){
                //Saber si existe algun select dentro de algun span
                var select = $(span).find('select');
                if(select.length > 0)
                    data[index] = $(select).val();//guardar valor de option
                else
                    data[index] = $(span).text();//guardar contenido texto
            }
            else{//Si no hay span guardamos el texto actual
                data[index] = $(el).text();//guardar contenido texto
            }
        });
        data.pop();
        data.pop();
        //Comprobar si realmente podemos añadir al carrito antes de cualquier gestion
        if(data[4] - data[5] >= 0){
            var json = {///Generar json con la informacion a enviar
                'id' : data[0],
                'marca' : data[1],
                'modelo' : data[2],
                'precio' : parseFloat(data[3]),//Parsear info string a valor numerico
                'stock' : parseInt(data[4]),
                'cantidad' : parseInt(data[5])
            };
            carritoModule.addItem(json);
        }
        else{
            console.log("No esta permitido el envio");
            //pintar warning msgModule diciendo que no tenemos stock suficiente
        }
    });
    //Eliminar item del carrito
    $(document).on('click', 'a.eliminar-item', function(event) {
        event.preventDefault();
        var id = $(this).children('img').eq(0).attr('alt');
        var json = { 'id' : id };
        carritoModule.deleteItem(json);
    });
    
    $(document).on('click', 'a.actualizar-item', function(event){
        event.preventDefault();
        var id = $(this).children('img').eq(0).attr('alt');
        //Redireccionar a pagian php que contiene los elementos de ese item
        window.location = registroModule.config.base_url + '/carrito/editarItem/' + id;
    });
    
    $('#cantidad').change(function(){
        var stock  = $('#stock').val();
        var cant = $("#cantidad").val();
        
        if((stock-cant) < 0){
            $('#cantidad').val(stock);
            $( "body" ).addClass('ui-widget-overlay');
            $("body").append('<div class="control_enabled">Debe introducir una cantidad validad, es decir inferior a la cantidad de stock</div>');
            $('.control_enabled').css({'display': 'block',
                                        'margin' : '0 auto'});
            $('.control_enabled').dialog({
                open: function() { $(".ui-dialog-titlebar-close").hide(); },
                title: 'Operación no permitida',
                content: 'Debe introducir una cantidad validad, es decir inferior a la cantidad de stock',
                modal: true,
                buttons: {
                  Cerrar: function() {
                    $( this ).dialog( "close" );
                    $( this ).remove('.control_enabled');
                    $("body").removeClass('ui-widget-overlay');
                  }
                }
            });
        }
        if(cant < 0){
            $('#cantidad').val(0);
            $( "body" ).addClass('ui-widget-overlay');
            $("body").append('<div class="control_enabled">Debe introducir una cantidad validad, es decir superior a 0</div>');
            $('.control_enabled').css({'display': 'block',
                                        'margin' : '0 auto'});
            $('.control_enabled').dialog({
                open: function() { $(".ui-dialog-titlebar-close").hide(); },
                title: 'Operación no permitida',
                content: 'Debe introducir una cantidad validad, es decir superior a 0',
                modal: true,
                buttons: {
                  Cerrar: function() {
                    $( this ).dialog( "close" );
                    $( this ).remove('.control_enabled');
                    $("body").removeClass('ui-widget-overlay');
                  }
                }
            });
        }
    });
    
    
    
    /* 
     * Formulario de registro
     * Gestionar provincias y pueblos mediante ajax
     */
    var URLactual = window.location;
    var registroModule = ({
        config : {
            base_url : window.location.origin,
            //base_url : window.location.origin + '/~tdiw-d7',
            initProvinciasURL : '/provincias',
            getPueblosURL : '/pueblos/getPueblo',
            getCpPueblosURL : '/pueblos/getCp'
        },
        initProvincias : (function(){
            $.ajax({
                url: registroModule.config.base_url + registroModule.config.initProvinciasURL,
                type: 'POST', 
                async: true, 
                cache: false, 
                error: function(response){
                    console.log("Hubo algun tipo de error");
                    //console.log(response);
                }, 
                success: function(data){
                    registroModule.mostrarDatos(data, 'provincias')
                }
            });
        }),
        getPueblos : (function(value){
            if(value != 0){
                $.ajax({
                    url: registroModule.config.base_url + registroModule.config.getPueblosURL,
                    type: 'POST', 
                    data : {'idSelected': value},
                    async: true, 
                    cache: false, 
                    error: function(response){
                        console.log("Hubo algun tipo de error");
                        //console.log(response);
                    }, 
                    success: function(data){
                        $('#pueblos').prop("disabled", false);
                        $('#pueblos').empty();
                        registroModule.mostrarDatos(data, 'pueblos');
                    }
                });
            }
            else{
                $('#pueblos').prop("disabled", true);
                $('#pueblos').empty();
                $('#pueblos').append('<option value="pueblo">-- Pueblos --</option>');
                $('#cp').val('');
            }
        }),
        getCP : (function(value){
            var cp = $('#cp');
            $.ajax({
                    url: registroModule.config.base_url + registroModule.config.getCpPueblosURL,
                    type: 'POST', 
                    data : {'idSelected': value},
                    async: true, 
                    cache: false, 
                    error: function(response){
                        console.log("Hubo algun tipo de error");
                        console.log(response);
                    }, 
                    success: function(data){
                        cp.val(data.cod_postal);
                    }
                });
        }),
        mostrarDatos : (function(data, id_select){
            var select = $('#' + id_select);
            var cp = $('#cp');
            
            if(data !== []){//Comprobar que no este vacio
                $.each(data, function(idx, values){
                    if(id_select === 'pueblos'){
                        select.append('<option value="' + values.id_pueblo + '">' + values.nombre + '</option>');
                        if(idx == 0){
                            cp.val(values.cod_postal);
                        }
                    }
                    else{
                        select.append('<option value="' + values.id_provincia + '">' + values.nombre + '</option>');
                    }
                });
                
            }
        })
    });
    //Comprobar que la url recibida es la encargada de cargar los formularios de 
    //inicio de sesion y registro para ejecutar ajax solo en esa pagina y poder cargar
    //las provincias automaticamente
    console.log(URLactual['href']);
    if(URLactual['href'] === (registroModule.config.base_url + "/usuarios") 
       || URLactual['href'] === (registroModule.config.base_url + "/usuarios/registro")
       || URLactual['href'] === (registroModule.config.base_url + "/usuarios/login")
       || URLactual['href'] === (registroModule.config.base_url + "/compraritems/nuevoCliente")
       || URLactual['href'] === (registroModule.config.base_url + "/compraritems/nuevocliente")){
        registroModule.initProvincias();
    }
    //Evento al cambiar opcion en selec de provincias
    $("#provincias").change(function(){
        var value = this.value;
        registroModule.getPueblos(value);
    });
    //Modificar el CP al modificar el pueblo
    $("#pueblos").change(function(){
       var value = this.value; 
       registroModule.getCP(value);
    });
    
    ///Gestionar elementos al crear un nuevo producto (adminpanel)
    //Mostrar tipo de producto segun categoria escogida
    var adminProductosModule = ({
        selectItem : (function(cat){
            var tipo = $("#tipo-item");
            var typeSelected = 0;
            tipo.empty();
            switch(cat){
                case '1':
                case '2':
                    typeSelected = 1;
                    tipo.append("<option value='tabla' selected='selected'>Tabla</option>");
                    tipo.append("<option value='ejes'>Ejes</option>");
                    tipo.append("<option value='ruedas'>Ruedas</option>");
                    tipo.append("<option value='cojinetes'>Cojinetes</option>");
                    adminProductosModule.selectDim(cat);
                    adminProductosModule._checkItem();
                    break;
                case '3':
                    typeSelected = 2;
                    tipo.append("<option value='lija' selected='selected'>Lija</option>");
                    tipo.append("<option value='gomas'>Gomas</option>");
                    tipo.append("<option value='tornillos'>Tornillos</option>");
                    tipo.append("<option value='tools'>Herramientas</option>");
                    tipo.append("<option value='protecciones'>Protecciones</option>");
                    tipo.append("<option value='cordones'>Cordones</option>");
                    adminProductosModule.ocultarInput("g-peso");
                    adminProductosModule.ocultarInput("g-dimension");
                    adminProductosModule.ocultarInput("g-concavo");
                    break;
                case '4':
                case '5':
                    typeSelected = 3;
                    tipo.append('<option value="sudaderas" selected="selected">Sudadera</option>')
                    tipo.append('<option value="camisetas">Camiseta</option>')
                    tipo.append('<option value="pantalones">Pantalones</option>')
                    adminProductosModule.ocultarInput("g-peso");                    
                    adminProductosModule.ocultarInput("g-concavo");
                    adminProductosModule.mostrarInput("g-dimension");
                    break;
                default:
                    tipo.append("<option value='default'>--Tipo item--</option>");
                    adminProductosModule.ocultarInput("g-peso");
                    adminProductosModule.ocultarInput("g-dimension");
                    adminProductosModule.ocultarInput("g-concavo");
                    break;
            }
            return typeSelected;
        }),
        selectDim : (function(value){
            var dim = $("#dimension");
            dim.empty();
            switch(value){
                case 'tabla':
                    var cat = $("#categorias");
                    if(cat.val() === "1"){
                        for(var j=30; j<51; j++){//Es algo guarro. lo suyo seria dos selects uno para longitud y otro para el ancho
                            for(var i=7; i<9; i++){
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".00\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".25\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".5\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".7\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".75\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".8\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".825\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".85\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".875\"</option>");
                            }
                        }
                    }
                    else if(cat.val() === "2"){
                        for(var j=30; j<66; j++){
                            for(var i=7; i<9; i++){
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".00\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".25\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".5\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".7\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".75\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".8\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".825\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".85\"</option>");
                                dim.append("<option value='" + j + "\"X" + i + "\"'>" + j + "\" X "+ i +".875\"</option>");
                            }
                        }
                    }
                    break;
                case 'ejes':
                    dim.append('<option value="7-7.50">7" - 7.5"</option>');
                    dim.append('<option value="7.50-8">7.5" - 8"</option>');
                    dim.append('<option value="8-8.5">8" - 8.5"</option>');
                    break;
                case 'ruedas':
                    for(var i=45; i<66; i++){
                        dim.append("<option value='" + i + "'>" + i + " mm</option>");
                    }
                    break;
                case 'camisetas':
                case 'sudaderas':
                case 'pantalones':
                    dim.append("<option value='XL'>XL</option>");
                    dim.append("<option value='L'>L</option>");
                    dim.append("<option value='M'>M</option>");
                    dim.append("<option value='S'>S</option>");
                    dim.append("<option value='XS'>XS</option>");
                    break;
                
                default:
                    dim.append("<option value='default'>--Seleccionar tipo--</option>");
                    break;
            }
            
        }),
        ocultarInput : (function(id){//Ocultar elementos
            $("#" + id).css({'display' : 'none'});
        }),
        mostrarInput : (function(id){//Mostrar elementos
            $("#" + id).css({'display' : 'block'});
        }),
        _checkItem : (function(){
            var dim = $("#tipo-item");
            switch(dim.val()){
                case 'tabla':
                    adminProductosModule.ocultarInput("g-peso");
                    adminProductosModule.mostrarInput("g-dimension");
                    adminProductosModule.mostrarInput("g-concavo");
                    break;
                case 'ejes':
                    adminProductosModule.ocultarInput("g-concavo");
                    adminProductosModule.mostrarInput("g-dimension");
                    adminProductosModule.mostrarInput("g-peso");
                    break;
                case 'cojinetes':
                    adminProductosModule.ocultarInput("g-peso");
                    adminProductosModule.ocultarInput("g-dimension");
                    adminProductosModule.ocultarInput("g-concavo");
                    break;
                case 'ruedas':
                    adminProductosModule.ocultarInput("g-peso");
                    adminProductosModule.ocultarInput("g-concavo");
                    adminProductosModule.mostrarInput("g-dimension");
            }
        })
    });
    
    $("#categorias").change(function(){
        var selected;
        selected = adminProductosModule.selectItem(this.value);
        switch(selected){
            case 1:
                adminProductosModule.selectDim('tabla');
                break;
            case 2:
                adminProductosModule.selectDim();
                break;
            case 3:
                adminProductosModule.selectDim('sudaderas');
                break;
        }
        adminProductosModule.ocultarInput();
        adminProductosModule.mostrarInput();
    });
    $("#tipo-item").change(function(){
        adminProductosModule.selectDim(this.value);
        adminProductosModule._checkItem();
    });
    
    /*
     * En desarrollo
     * Pintar elementos en su div
     * Error
     * Warning
     * Succses
     */
    var msgModule = ({
        error : (function(data){
            var parent = $('#notifications');
            
        }),
        warning : (function(data){
            var parent = $('#notifications');
            
        }),
        success : (function(data){
            var parent = $('#notifications');
            
        })
    });
    
    //Datepicker de jquery-ui
    $('#datepicker').datepicker();
    
//})(jQuery);
});

