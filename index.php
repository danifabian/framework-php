<?php
//Esconder los errores
//error_reporting(E_ALL);
//ini_set('display_errors', 'on');
/**
 * index.php es el fichero principal de la aplicacion
 * y se encarga de dclarar las constantes basicas
 * y llamar a bootstras para que empiece a funciona la aplicacion.
 */
define('DS', DIRECTORY_SEPARATOR);

define('ROOT', realpath(dirname(__FILE__)) . DS);
define('CORE_PATH', ROOT . '_system' . DS . 'core' . DS);
define('LIBS_PATH', ROOT . '_system' . DS . 'libs' . DS);

try{
    require_once (LIBS_PATH . 'fb.php');//Debugar con FIrebug en Firefox
    require_once (CORE_PATH . 'Autoload.php');
    require_once CORE_PATH . 'Config.php';
    require_once CORE_PATH . 'Constants.php';
    
    Session::init();
    
    //Log::myLog('Esto es una prueba de log');
    $registry = Registry::getInstancia();
    $registry->_request = new Request();
    $registry->_db = new Database(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD, DB_CHARSET);
    Bootstrap::run($registry->_request);
}
catch(Exception $e){
    echo $e->getMessage();
}

//Eliminar (si descomentamos hace conflicto con ajax del carrito)
//echo '<pre>';
//print_r(get_required_files());
